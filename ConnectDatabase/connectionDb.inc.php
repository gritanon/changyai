  <?php

define('DB_HOST','localhost');
// define('DB_NAME','db_meeyer');
// define('DB_USERNAME','root');
// define('DB_PASSWORD','root');
define('ROOTPATH',curRootPath('admin'));

//host
define('DB_NAME','thaitest_changyai');
define('DB_USERNAME','thaitest_changyai');
define('DB_PASSWORD','get0831535774');

// Create connection
require_once('MySQLDBConn.class.php');
$conn = new MySQLDBConn(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);

function getDbConnection() {
    return new MySQLDBConn(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
}

function curRootPath($localhost_path,$server_name='localhost')
{
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"])) if($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    //$server_name = $_SERVER["SERVER_NAME"];
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $server_name.":".$_SERVER["SERVER_PORT"].'/'.$localhost_path;
    }
    else if($_SERVER["SERVER_PORT"] == "80" && ($server_name == 'localhost' || $server_name == '127.0.0.1')){
        $pageURL .= $server_name.'/'.$localhost_path;
    }
    else
    {
        $pageURL .= $server_name;
    }
    return $pageURL;
}
// echo "<script> alert('Please check username and password'); history.back(); </script>";

// echo "Connected successfully";
function getIsset($post_value){
    $value="";
    if(isset($_GET[$post_value])){
        $value = $_GET[$post_value];
    }
    if(isset($_POST[$post_value])){
        $value = $_POST[$post_value];
    }
    return $value;
}

function redirectTo($url){
  header('location: '.$url);
  exit(0);
}

function alertMassage($str){
    echo "<script>alert('".$str."');</script>";
}

function alertMassageAndRedirect($str,$url){
  	echo "<script> alert('".$str."'); location.href='".$url."'; </script>";
}

function confirmMassage($str){
    echo "<script>confirm('".$str."');</script>";
}

function generate_action_tag($type,$message){
    $tag = '';
    if('success' == $type){
        $tag = '<div class="alert alert-success alert-dismissible">
                        <h4><i class="icon fa fa-check"></i>'.$message.'</h4>
                    </div>';
    }
    if('error' == $type){
        $tag = '<div class="alert alert-danger alert-dismissible">
                        <h4><i class="icon fa fa-ban"></i>'.$message.'</h4>
                    </div>';
    }
    return $tag;
}

//transfer
function InsertTransferDetail($transferId,$Name,$Price) {
    $conn = getDbConnection()->getConnection();
    $sql = "INSERT INTO transfer_detail (transferId,Name,Price) VALUES ('$transferId','$Name','$Price')";
    $conn->query("SET NAMES UTF8");
    $conn->query($sql);
}

function DeleteTransferDetail($transferId) {
    $conn = getDbConnection()->getConnection();
    $sql = "DELETE From transfer_detail WHERE transferId = $transferId";
    $conn->query("SET NAMES UTF8");
    $conn->query($sql);
}

//move
function InsertMoveDetail($moveId,$Name,$Price) {
    $conn = getDbConnection()->getConnection();
    $sql = "INSERT INTO move_detail (moveid,Name,Price) VALUES ('$moveId','$Name','$Price')";
    $conn->query("SET NAMES UTF8");
    $conn->query($sql);
}

function DeleteMoveDetail($moveId) {
    $conn = getDbConnection()->getConnection();
    $sql = "DELETE From move_detail WHERE moveid = $moveId";
    $conn->query("SET NAMES UTF8");
    $conn->query($sql);
}

//change
function InsertChangeDetail($changeid,$Name,$Price) {
    $conn = getDbConnection()->getConnection();
    $sql = "INSERT INTO change_detail (changeid,Name,Price) VALUES ('$changeid','$Name','$Price')";
    $conn->query("SET NAMES UTF8");
    $conn->query($sql);
}

function DeleteChangeDetail($changeid) {
    $conn = getDbConnection()->getConnection();
    $sql = "DELETE From change_detail WHERE changeid = $changeid";
    $conn->query("SET NAMES UTF8");
    $conn->query($sql);
}

function UpdateFollow($cusID,$carID,$startDate,$endDate) {
    $conn = getDbConnection()->getConnection();
    $sql = "UPDATE follow SET startDate = '$startDate' , endDate = '$endDate' WHERE cusID = $cusID and carID = $carID";
    $conn->query("SET NAMES UTF8");
    $conn->query($sql);
}


function convertToDateThai($dateSelect) {
    $year = date("Y",strtotime($dateSelect))+543;
		$month = date("n",strtotime($dateSelect));
		$day = date("j",strtotime($dateSelect));

    if (strlen($month) < 2) {
      $month = '0'.$month;
    }

    if (strlen($day) < 2) {
      $day = '0'.$day;
    }

    return $year.'-'.$month.'-'.$day;
}

// function convertDateThai($strDate='now')
function convertDateThai($strDate)
{
    $strYear = substr($strDate,0,4); //date("Y",strtotime($strDate1));
    $strMonth= substr($strDate,5,2); //date("n",strtotime($strDate));
    $strDay= substr($strDate,8,2); //date("j",strtotime($strDate));
//substr ตัดคำ พารามิเตอร์คือ อักษรที่ต้องการให้เริ่มแสดง,ต้องการให้แสดงกี่ตัวอักษร
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[intval($strMonth)];
    return "$strDay $strMonthThai $strYear";
}





    function convertDate($strDate)
    {
        $unix_date = ($strDate - 25569) * 86400;
        $excel_date = 25569 + ($unix_date / 86400);
        $unix_date = ($excel_date - 25569) * 86400;

        return gmdate("Y-m-d", $unix_date);
    }

    // วัน/เดือน/ปี ให้กลายเป็น ปี-เดือน-วัน
    function convertToYmd($strDate)
    {
        $strYear = substr($strDate,6,4);
        $strMonth= substr($strDate,3,2); 
        $strDay= substr($strDate,0,2); //date("j",strtotime($strDate));
    //substr ตัดคำ พารามิเตอร์คือ อักษรที่ต้องการให้เริ่มแสดง,ต้องการให้แสดงกี่ตัวอักษร
        $strMonthThai=$strMonthCut[intval($strMonth)];
        return "$strYear-$strMonth-$strDay ";
    }


    function convertTomdY($strDate)
    {
        $strYear = substr($strDate,6,4);
        $strMonth= substr($strDate,3,2); 
        $strDay= substr($strDate,0,2); //date("j",strtotime($strDate));
    //substr ตัดคำ พารามิเตอร์คือ อักษรที่ต้องการให้เริ่มแสดง,ต้องการให้แสดงกี่ตัวอักษร
        $strMonthThai=$strMonthCut[intval($strMonth)];
        return "$strDay-$strMonth-$strYear ";
    }
?>
