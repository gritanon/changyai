	<?php 
		// function convertDateThai($strDate='now')
		function convertDateThai($strDate)
		{
			$strYear = substr($strDate,0,4); //date("Y",strtotime($strDate1));
			$strMonth= substr($strDate,5,2); //date("n",strtotime($strDate));
			$strDay= substr($strDate,8,2); //date("j",strtotime($strDate));
		//substr ตัดคำ พารามิเตอร์คือ อักษรที่ต้องการให้เริ่มแสดง,ต้องการให้แสดงกี่ตัวอักษร
			$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
			$strMonthThai=$strMonthCut[intval($strMonth)];
			return "$strDay $strMonthThai $strYear";
		}

		function convertToDateThai($dateSelect) {
			$year = date("Y",strtotime($dateSelect))+543;
				$month = date("n",strtotime($dateSelect));
				$day = date("j",strtotime($dateSelect));
		
			if (strlen($month) < 2) {
			  $month = '0'.$month;
			}
		
			if (strlen($day) < 2) {
			  $day = '0'.$day;
			}
		
			return $year.'-'.$month.'-'.$day;
		}
	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="apple-touch-icon" sizes="180x180" href="index/img/favicons/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="index/img/favicons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="index/img/favicons/favicon-16x16.png">
		<link rel="shortcut icon" href="index/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="codepixer">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ตรอ. ช่างใหญ่เซอร์วิส</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
		<link href="https://fonts.googleapis.com/css?family=Mitr" rel="stylesheet">
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="index/css/linearicons.css">
			<link rel="stylesheet" href="index/css/font-awesome.min.css">
			<link rel="stylesheet" href="index/css/bootstrap.css">
			<link rel="stylesheet" href="index/css/magnific-popup.css">
			<link rel="stylesheet" href="index/css/nice-select.css">					
			<link rel="stylesheet" href="index/css/animate.min.css">
			<link rel="stylesheet" href="index/css/owl.carousel.css">
			<link rel="stylesheet" href="index/css/main.css">
		</head>
		<style>
			html,body{
				font-family: 'Mitr', sans-serif;
				height: 100%;
			}
			size-b{
				color: #005c9e;
				font-size: 2.1em;
				font-weight: bolder;
			}
		</style>
		<body>
		

			  <header id="header" id="home">
			    <div class="container">
			    	<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				        <a href="index.html"><img src="index/img/logoChangyai.png" alt="" title="" /></a>
				      </div>
				      <nav id="nav-menu-container">
				        <ul class="nav-menu">
						  <li class="menu-active"><a href="#home">หน้าแรก</a></li>
						  <li><a href="#promotion">โปรโมชั่น</a></li>
				          <li><a href="#news">ข่าวสาร</a></li>
				          <li><a href="#download">ดาวโหลด</a></li>
				          <li><a href="#assessment">ประเมินการบริการ</a></li>
						  <li><a href="#contact">ติดต่อ</a></li>
						  <li><a href="login.php">เข้าสู่ระบบ</a></li>
				        </ul>
				      </nav><!-- #nav-menu-container -->		    		
			    	</div>
			    </div>
			  </header><!-- #header -->

			<!-- start banner Area -->
			<section class="banner-area relative p-0" id="home">
				<div class="container">
					<div class="row fullscreen d-flex align-items-center justify-content-end">
						<div class="banner-content col-lg-7 col-md-12">
							<h1>
								Changyai Service			
							</h1>
							<p>
							สถานตรวจสภาพรถเอกชนโดยจะตรวจสภาพรถยนต์ รถจักรยานยนต์      ก่อนเสียภาษีรถประจำปีซึ่งสถานตรวจสภาพรถเอกชน หรือตรวจ ตรอ. มีวัตถุประสงค์              เพื่อความปลอดภัยในการใช้รถใช้ถนนและลดมลภาวะอันเกิดจากรถ รวมทั้งความมั่นคงแข็งแรง    และอุปกรณ์ส่วนควบให้อยู่ในสภาพที่เหมาะสมก่อนนำไปใช้งาน 
							</p>
						</div>												
					</div>
				</div>
			</section>
			<!-- End banner Area -->	

			<!-- Start testimonial Area -->
			<section class="testimonial-area relative ">
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-60 col-lg-8">
							<div class="title text-center">
								<h1 class="mt-5 text-white" id="promotion">Promotion พิเศษ</h1>
							</div>
						</div>
					</div>						
					<div class="row">
						<div class="active-testimonial mb-5">
							<?php 
								require_once('php/connect.php');
								$sql = "SELECT * FROM promotion ";
								$result = $conn->query($sql) or die($conn->error);
								$Num_Rows=mysqli_num_rows($result);
								$i=1;
								while($row = $result->fetch_assoc()) {  		
							?>

							<div class="single-testimonial item d-flex flex-row">
								<div class="desc">
								<a href="promotion.php?id=<?php echo $row['id']  ?>" title="อ่านเพิ่มเติม"><img class="img-fluid d-block mx-auto" src="pages/promotion/promotion/<?php echo $row['img']  ?>" width="150px" alt=""></a>
									<h4 mt-30 title="อ่านเพิ่มเติม"> <a href="promotion.php?id=<?php echo $row['id']  ?>" title="อ่านเพิ่มเติม"></i></a><?php echo $row['name']  ?></a></h4>
									<p><?php echo convertDateThai(convertToDateThai($row['create_at'])); ?></p>
								</div>
							</div> 
							<?php
								$i++;
								}
							?>								
						</div>					
					</div>
				</div>	
			</section>
			<!-- End testimonial Area -->
			
			<!-- Start news Area --> 
			<section class="sample-text-area" id="news">
				<div class="container">
					<font size="6px" color="#005c9e" class="size-b">ข่าวสาร</font>
					<?php 
						require_once('php/connect.php');
						$sql = "SELECT * FROM news ";
						$result = $conn->query($sql) or die($conn->error);
						$Num_Rows=mysqli_num_rows($result);

						$i=1;
						while($row = $result->fetch_assoc()) {  		
					?>
					<hr>
					<font size="5" color="black"><?php echo $row['topic']  ?></font><br>
					เมื่อวันที่ <storng><?php echo convertDateThai(convertToDateThai($row['create_at'])); ?></storng>
					<p class="sample-text">
						<?php echo $row['detail']  ?>
						<a href="<?php echo $row['link']  ?>">Read more</a>
					</p>
					<?php
						$i++;
						}
					?>
					<hr id="download">							
				</div>	
			</section>
			<!-- End news Area -->

			<!-- Start testimonial Area -->
			<section >
				<div class="container">
						<div class="mb-30">
							<font size="6px" color="#005c9e" class="size-b">ดาวโหลดเอกสาร</font>
						</div>
						<div class="progress-table-wrap">
							<div class="progress-table">
								<div class="table-head">
									<div class="serial">#</div>
									<div class="country">ชื่อเอกสาร</div>
									<div class="country">วันที่สร้าง</div>
								</div>
								<?php 
									require_once('php/connect.php');
									$sql = "SELECT * FROM document ";
									$result = $conn->query($sql) or die($conn->error);
									$Num_Rows=mysqli_num_rows($result);

									$i=1;
									while($row = $result->fetch_assoc()) {  

									$name = $row['name'];
									$file = $row['file'];
									
								?>
								<div class="table-row">
									<div class="serial"><?=$i?></div>
									<div class="country"><?=$name?></div>
									<div class="country"><?php echo convertDateThai(convertToDateThai($row['create_at'])); ?></div>
									<div class="country"> <a href="pages/file/file/<?=$file?>">ดาวโหลด</a></div>
								</div>
								<?php
									$i++;
									}
								?>
							</div>
						</div>
					</div>
				</div>	
			</section>
			<!-- End testimonial Area -->
			<section class="contact-area section-gap" >
			<div class="container" id="assessment">
				<div class="mb-30"  >
					<font size="6px" color="#005c9e" class="size-b" >แบบประเมินการบริการ</font>
				</div>
				<form method="post" name="frmMain" action="save.php" OnSubmit="return fncSubmit();">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead >
								<tr>
									<td rowspan="2"  align="center" style="padding-top: 2rem;">ลำดับ</td>
									<td rowspan="2" align="center" style="padding-top: 2rem;">คำถาม</td>
									<td colspan="5" align="center">ลำดับความพึงพอใจ</td>
								</tr>
								<tr>
									<td  align="center" >5</td>
									<td  align="center" >4</td>
									<td  align="center" >3</td>
									<td  align="center" >2</td>
									<td  align="center" >1</td>
								</tr>
						</thead>
						<tbody>
								<tr>
								<?php 
									require_once('php/connect.php');
									$sql = "SELECT * FROM tb_question ";
									$result = $conn->query($sql) or die($conn->error);
									$Num_Rows=mysqli_num_rows($result);

									$i=1;
									while($row = $result->fetch_assoc()) {  
									$id_chk = $row['id_question']; //รหัสคำถาม
									$name = $row['question']; // ชื่อคำถาม
								?>
								<!-- Start assessment Area -->	
									<td  align="center">
									<?=$i?>
									</td>
									<td>
										<?=$name?>
									</td>
									<td align="center"><input name="radionNo<?=$i;?>" id="radionNo<?=$i;?>_1" type="radio" value="5"></td>
									<td align="center"><input name="radionNo<?=$i;?>" id="radionNo<?=$i;?>_2" type="radio" value="4"></td>
									<td align="center"><input name="radionNo<?=$i;?>" id="radionNo<?=$i;?>_3" type="radio" value="3"></td>
									<td align="center"><input name="radionNo<?=$i;?>" id="radionNo<?=$i;?>_4" type="radio" value="2"></td>
									<td align="center"><input name="radionNo<?=$i;?>" id="radionNo<?=$i;?>_5" type="radio" value="1"></td>
								</tr>
								<?php
									$i++;
									}
								?>

						</tbody>
					</table>


				<div id="recaptcha-wrapper" class="text-center my-2">
					<div class="g-recaptcha d-inline-block" data-callback="recaptchaCallback" data-sitekey="6LfLWGkUAAAAAIIEo9XZRZVeZSIERI3fF3JkKP0x"></div>
				</div>
				<input type="hidden" name="index" value="<?php echo $Num_Rows ?>" class="form-control" > 
				<input type="hidden" name="hdnRows" value="<?=$i-1;?>">
				<button type="submit" id="btn-submit" name="btn-submit" class="btn btn-primary d-block mx-auto" /disabled>ส่งแบบประเมิน</button>
				</form>
			</div>
			</section>
			<!-- end assessment Area -->		

		
		
			<!-- start footer Area -->		
			<?php 
				require_once('php/connect.php');
				$sql = "SELECT * FROM contact ";
				$result = $conn->query($sql) or die($conn->error);
				$Num_Rows=mysqli_num_rows($result);


				$row = $result->fetch_assoc();		
			?>
			<footer class="footer-area section-gap" id="contact">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h4 class="text-white">About Us</h4>
								<p>
								  รับตรวจสภาพรถ ทำ พ.ร.บ. ประกันรถยนต์ ต่อภาษีรถยนต์ทุกประเภท โอน ย้าย เปลี่ยนสี เครื่องยนต์
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h4 class="text-white">Contact Us</h4>
								<p>
							     	<?php echo $row['address']  ?>		
								</p>
							</div>
						</div>						
						<div class="col-lg-5 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h4 class="text-white">Newsletter</h4>
								<p>You can trust us. we only send  offers, not a single spam.</p>
								<div class="d-flex flex-row" id="mc_embed_signup">
									  <form class="navbar-form" novalidate="true" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get">
									    <div class="input-group add-on">
									      	<input class="form-control" name="EMAIL" placeholder="Email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email address'" required="" type="email">
											<div style="position: absolute; left: -5000px;">
												<input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
											</div>
									      <div class="input-group-btn">
									        <button class="genric-btn"><span class="lnr lnr-arrow-right"></span></button>
									      </div>
									    </div>
									      <div class="info mt-20"></div>									    
									  </form>
								</div>
							</div>
						</div>				
					</div>
					
					<div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            <p class="footer-text m-0">Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						<div class="footer-social d-flex align-items-center">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-dribbble"></i></a>
							<a href="#"><i class="fa fa-behance"></i></a>
						</div>
					</div>
				</div>
			</footer>	
			<!-- End footer Area -->
			<?php ?>
										
			<!-- facebook plugin -->
			<div class="fb-customerchat" page_id="2448536265374799"></div>

			<script src="index/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="index/js/vendor/bootstrap.min.js"></script>			
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="index/js/easing.min.js"></script>			
			<script src="index/js/hoverIntent.js"></script>
			<script src="https://www.google.com/recaptcha/api.js"></script>
			<script src="index/js/superfish.min.js"></script>	
			<script src="index/js/jquery.ajaxchimp.min.js"></script>
			<script src="index/js/jquery.magnific-popup.min.js"></script>	
			<script src="index/js/owl.carousel.min.js"></script>			
			<script src="index/js/jquery.sticky.js"></script>
			<script src="index/js/jquery.nice-select.min.js"></script>	
			<script src="index/js/waypoints.min.js"></script>
			<script src="index/js/jquery.counterup.min.js"></script>					
			<script src="index/js/parallax.min.js"></script>	
			<script src="index/js/mail-script.js"></script>	
			<script src="index/js/main.js"></script>	

			<!-- recaptcha -->
			<script>
				$(function(){
					// global variables
					captchaResized = false;
					captchaWidth = 304;
					captchaHeight = 78;
					captchaWrapper = $('#recaptcha-wrapper');
					captchaElements = $('#rc-imageselect, .g-recaptcha');

					$(window).on('resize', function() {
						resizeCaptcha();
					});

					resizeCaptcha();
				});

				function resizeCaptcha() {
					if (captchaWrapper.width() >= captchaWidth) {
						if (captchaResized) {
							captchaElements.css('transform', '').css('-webkit-transform', '').css('-ms-transform', '').css('-o-transform', '').css('transform-origin', '').css('-webkit-transform-origin', '').css('-ms-transform-origin', '').css('-o-transform-origin', '');
							captchaWrapper.height(captchaHeight);
							captchaResized = false;
						}
					} else {
						var scale = (1 - (captchaWidth - captchaWrapper.width()) * (0.05/15));
						captchaElements.css('transform', 'scale('+scale+')').css('-webkit-transform', 'scale('+scale+')').css('-ms-transform', 'scale('+scale+')').css('-o-transform', 'scale('+scale+')').css('transform-origin', '0 0').css('-webkit-transform-origin', '0 0').css('-ms-transform-origin', '0 0').css('-o-transform-origin', '0 0');
						captchaWrapper.height(captchaHeight * scale);
						if (captchaResized == false) captchaResized = true;
					}
				}
				// resizeCaptcha();

				function recaptchaCallback () {
					$('#btn-submit').removeAttr('disabled');
				}
			</script>

			<!-- FACEBOOK Plugin -->
			<script>
				window.fbAsyncInit = function() {
					FB.init({
					appId            : '223252098389318',
					autoLogAppEvents : true,
					xfbml            : true,
					version          : 'v3.1'
					});
				};

				(function(d, s, id){
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) {return;}
					js = d.createElement(s); js.id = id;
					js.src = "https://connect.facebook.net/en_US/sdk.js";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));

					function fncSubmit()
					{
					
						var Rows = document.frmMain.hdnRows.value;
						for(x=1;x<=Rows;x++)
						{
							var op1 = document.getElementById("radionNo"+x+"_1");
							var op2 = document.getElementById("radionNo"+x+"_2");
							var op3 = document.getElementById("radionNo"+x+"_3");
							var op4 = document.getElementById("radionNo"+x+"_4");
							var op5 = document.getElementById("radionNo"+x+"_5");
							if(op1.checked == false && op2.checked == false && op3.checked == false  && op4.checked == false  && op5.checked == false)
							{
								alert('กรุณาใส่คะแนนแบบสำรวจข้อที่ ' + x + ' เพื่อดำเนินการต่อ');
								return false;
							}
						}

					}


			</script>
    
		</body>
	</html>



