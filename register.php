<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition register-page">
<div class="register-box mt-3">
  <div class="register-logo">
    <a href="../../index2.html"><img src="../../dist/img/logochangyai-128x128.png" alt="User Avatar" class="img-circle">
</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">ลงทะเบียนเจ้าของกิจการ</p>

      <form action="../../index.html" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username">
          <div class="input-group-append">
              <span class="fa fa-user input-group-text"></span>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
              <span class="fa fa-lock input-group-text"></span>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Retype password">
          <div class="input-group-append">
              <span class="fa fa-lock input-group-text"></span>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="ชื่อ-นามสกุล">
          <div class="input-group-append">
              <span class="fa fa-user input-group-text"></span>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" data-inputmask='"mask": "(999) 999-9999"' placeholder="โทรศัพท์" data-mask>
          <div class="input-group-prepend">
              <span class="input-group-text"><i class="fa fa-phone"></i></span>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="texy" class="form-control" placeholder="ตรอ.">
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="ที่อยู่">
        </div>
        <div class="row mb-3">
            <div class="col-6">
                <select class="form-control">
                    <option>จังหวัด</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                </select>
            </div>
            <div class="col-6">
                <select class="form-control">
                    <option>อำเภอ</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <select class="form-control">
                    <option>เลือกคำถาม</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                </select>
            </div>
            <div class="col-6">
                <input type="text" class="form-control" placeholder="ใส่คำตอบ">
            </div>
        </div>
        <div class="input-group mb-3">
            <font color="red">*กรุณากรอกเพื่อใช้กรณีลืมรหัสผ่าน</font>
        </div>
        <div class="input-group mb-3">
          <div class="btn btn-block btn-primary">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
          </div>
        </div>
      </form>

      <a href="login.php" class="text-center">มีรหัสสมาชิกแล้วต้องการเข้าสู่ระบบ</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })
</script>
</body>
</html>
