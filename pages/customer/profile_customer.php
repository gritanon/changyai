<?php 
    require_once('../../php/connect.php');
      $sql = "SELECT * FROM customer WHERE id = '".$_GET['id']."'";
      $result = $conn->query($sql) or die($conn->error);

      if ($result->num_rows > 0){
          $row = $result->fetch_assoc();
      } else {
          header('Location: ../customer');
      }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Customer Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
<!-- Navbar & Main Sidebar Container -->
<?php include_once('../includes/sidebar.php') ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Customer</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="../customer">ข้อมูลลูกค้า</a></li>
              <li class="breadcrumb-item active">รายละเอียดลูกค้า</li>
            </ol>
          </div>
        </div>
      </div>
    <!-- /.container-fluid -->
    </section>
    <section class="content">
      <div class="card">
            <!-- Main content -->
            <div class="card-header">
              <h3 class="card-title d-inline-block"> ตรอ. ช่างใหญ่ เซอร์วิส</h3>
              <h5 class="float-right">วันที่ <?php echo date_format(new DateTime($row['created_at']),"d/m/Y");  ?></h5>
            </div>
            <div class="card-body">
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    ทะเบียนรถ <?php echo $row['car'] ?><br>
                    เจ้าของรถ toyota******<br>
                    ชื่อ <?php echo $row['name'] ?><br>
                    เบอร์โทร <?php echo $row['phone'] ?><br>
                    วันที่จดทะเบียน <?php echo date_format(new DateTime($row['registration']),"d/m/Y");  ?>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    จังหวัด <?php echo $row['province'] ?><br>
                    รุ่น MTX****<br>
                    ที่อยู่ <?php echo $row['address'] ?><br>
                    ชนิดเชื้อเพลิง ___________<br>
                    วันที่เสียภาษี __________
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    รย. <?php echo $row['type'] ?><br>
                    หมายเลขตัวถัง <?php echo $row['address'] ?><br>
                    หมายเลขเครื่อง ___________<br>
                    หมายเลขถังแก๊ส ___________<br>
                    วัน พรบ. หมด <?php echo date_format(new DateTime($row['act']),"d/m/Y");  ?>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.invoice -->
      </div>
    </section>
    <!-- /.content -->
    <?php 
        $sql = "SELECT * FROM orders WHERE UserID = '".$_GET['id']."'";
        $result_customer = $conn->query($sql) or die($conn->error);
    ?>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title d-inline-block">History</h3>
          <a href="create_service.php?id=<?php echo $row['id'] ?>">
            <button type="button" class="btn btn-primary btn-xm float-right">
              <i class="fa fa-edit"></i> เพิ่มข้อมูลการตรวจ
            </button>
          </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive">
          <table id="dataTable" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>ID.</th>
                <th>วันที่ใช้บริการ</th>
                <th>ผู้บันทึก</th>
                <th>ผู้ติดตาม</th>
                <th>ค่าใช้จ่ายทั้งหมด</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            <?php while($row = $result_customer->fetch_assoc()) {  ?>
              <tr>
                <td><?php echo $row['OrderID'] ?></td>
                <td><?php echo date_format(new DateTime($row['OrderDate']),"d/m/Y"); ?></td>
                <td>นาย พบใจ เสมสุข</td>
                <td>นาย สมใจ บุญมา</td>
                <td><?php echo $row['Total'] ?> บาท</td>
                <td>
                  <a href="detail_service.php?OrderID=<?php echo $row['OrderID'] ?>" class="btn btn-sm btn-primary text-white">
                    เพิ่มเติม
                  </a> 
                </td>
                <td>
                  <a href="invoice-print.php?OrderID=<?php echo $row['OrderID'] ?>" class="btn btn-sm btn-primary text-white">
                    พิมพ์ใบเสร็จ
                  </a> 
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
    </section>
    <!-- /.content -->
                    
  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>
  
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- daterangepicker -->
<script src="../../plugins/moment/min/moment.min.js"></script>
<script src="../../plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/locales/js/bootstrap-datepicker.th.js"></script>
<script src="../../plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
</body>
</html>
