<?php

session_start();
require_once "../../ConnectDatabase/connectionDb.inc.php";

$id = getIsset("id");
$cmd = getIsset("__cmd");

$sql = "SELECT a.id,a.cusID,a.carID,a.Date,a.type,a.Price,a.startDate,a.endDate,c.license,c.province_license
FROM act a inner join car c on a.carID = c.id WHERE a.cusID='$id'";
$select_act = $conn->queryRaw($sql);
$total_act = sizeof($select_act);

$sql2 = "SELECT cc.id,cc.cusID,cc.carID,cc.Date,cc.ConditionResults,cc.Remark,cc.Price,c.license,c.province_license
FROM car_check cc inner join car c on cc.carID = c.id WHERE cc.cusID='$id'";
$select_checkcar = $conn->queryRaw($sql2);
$total_checkcar = sizeof($select_checkcar);

$sql3 = "SELECT ch.id,ch.cusID,ch.carID,ch.Date,ch.license_new,ch.province_license_new,ch.remark,c.license,c.province_license,sum(cd.Price) as qtyprice
FROM change_car ch inner join car c on ch.carID = c.id inner join change_detail cd on cd.changeid = ch.id WHERE ch.cusID='$id'";
$select_change = $conn->queryRaw($sql3);
$total_change = sizeof($select_change);

$sql4 = "SELECT t.id,t.cusID,t.carID,t.Date,t.total,t.endDate,c.license,c.province_license
FROM tax t inner join car c on t.carID = c.id WHERE t.cusID='$id'";
$select_tax = $conn->queryRaw($sql4);
$total_tax = sizeof($select_tax);

$sql5 = "SELECT i.id,i.cusID,i.carID,i.Date,i.premium,i.endDate,i.type,i.insurance,c.license,c.province_license
FROM insurance i inner join car c on i.carID = c.id WHERE i.cusID='$id'";
$select_insurance = $conn->queryRaw($sql5);
$total_insurance = sizeof($select_insurance);

$sql6 = "SELECT tr.id,tr.cusID,tr.carID,tr.Date,tr.RFName,tr.RLName,tr.RTel,tr.RAddress,tr.Rdistrict,tr.Ramphoe,tr.Rprovince,tr.Rzipcode,c.license,c.province_license,sum(td.Price) as qtyprice
FROM transfer tr inner join car c on tr.carID = c.id inner join transfer_detail td on td.transferid = tr.id WHERE tr.cusID='$id'";
$select_transfer = $conn->queryRaw($sql6);
$total_transfer = sizeof($select_transfer);

$sql7 = "SELECT m.id,m.cusID,m.carID,m.Date,m.source,m.destination,c.license,c.province_license,sum(md.Price) as qtyprice 
FROM move m inner join car c on m.carID = c.id inner join move_detail md on md.moveid = m.id WHERE m.cusID='$id'";
$select_move = $conn->queryRaw($sql7);
$total_move = sizeof($select_move);

$sql8 = "SELECT c.id,c.cusID,c.body_number,c.id_document,c.license,c.province_license
FROM car c WHERE c.cusID='$id'";
$select_car = $conn->queryRaw($sql8);
$total_car = sizeof($select_car);


if (intval($id) > 0){ 
  //Update 
  if ($cmd == 'save') {
    $value = array(
        "FName"=>getIsset('FName')
         ,"LName"=>getIsset('LName')
         ,"Address"=>getIsset('Address')
         ,"district"=>getIsset('district')
         ,"amphoe"=>getIsset('amphoe')
         ,"province"=>getIsset('province')
         ,"zipcode"=>getIsset('zipcode')
         ,"Tel"=>getIsset('Tel')
    );

    if ($conn->update("customer", $value, array("id" => $id))) {
        alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','index.php');
    }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
    }

  }else{

    $tbl_cus = $conn->select('customer', array('id' => getIsset('id')), true);

    if($tbl_cus != null){
      $FName = $tbl_cus["FName"];
      $LName = $tbl_cus["LName"];
      $Address = $tbl_cus["Address"];
      $district = $tbl_cus["district"];
      $amphoe = $tbl_cus["amphoe"];
      $province = $tbl_cus["province"];
      $zipcode = $tbl_cus["zipcode"];
      $Tel = $tbl_cus["Tel"];
    }

    
  }
}else{
  //Insert
  if ($cmd == 'save') {

    $value = array(
      "FName"=>getIsset('FName')
       ,"LName"=>getIsset('LName')
       ,"Address"=>getIsset('Address')
       ,"district"=>getIsset('district')
       ,"amphoe"=>getIsset('amphoe')
       ,"province"=>getIsset('province')
       ,"zipcode"=>getIsset('zipcode')
       ,"Tel"=>getIsset('Tel')
    );
     if($conn->create("customer",$value)){
        alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','index.php');
     }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
     }

  }
}

if($cmd == 'delete'){
  //delete
    if($conn->delete("customer",array("id"=>$id))){
        alertMassageAndRedirect('ลบสำเร็จ','index.php');
    }else{
        alertMassage("ลบไม่สำเร็จ");
    }
}


?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
  <!-- address -->
  <link rel="stylesheet" href="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dist/jquery.Thailand.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/check_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <form  id="demo1" class="demo" style="display:none;" autocomplete="off" uk-grid >
      <div class="container-fluid">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title  mt-1 mb-2">ข้อมูลลูกค้า</h3>
          </div>
          <div class="col-12 table-responsive card-body">
            <div class="card-body">
            <b class="">รายละเอียดข้อมูลลูกค้า</b>
            <hr>
              <input type="hidden" name="id" value="<?php echo $id ?>">
              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" >ชื่อ</label>
                <div class="col-10">
                  <input type="text" class="form-control" placeholder="ชื่อ" name="FName" value="<?php echo $FName; ?> <?php echo $LName; ?>">
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" >เบอร์โทร</label>
                <div class="col-10">
                  <input type="number" class="form-control" placeholder="เบอร์โทร" name="Tel" value="<?php echo $Tel; ?>" maxlength="10">
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" >ที่อยู่</label>
                <div class="col-10">
                  <input type="text" class="form-control" placeholder="บ้านเลขที่" name="Address" value="<?php echo $Address; ?> ต.<?php echo $district; ?> อ.<?php echo $amphoe; ?> จ.<?php echo $province; ?> <?php echo $zipcode; ?>">
                </div>
              </div>

            <?php

                        $index =0;
                                foreach ($tbl as $row) {
                                    $index++;
                                    $SumTotal = $SumTotal + $row['Total'];
                                    ?>
          <tr>
              <td><input type="text" name="txtName<?php echo $index ?>" value="<?php echo $row['Description']; ?>"></td>
              <td><input  type="number" name="txtPrice<?php echo $index ?>" id="txtPrice<?php echo $index ?>" value="<?php echo $row['Count']; ?>" OnKeyUp="calculateAmount(<?php echo $index ?>)"></td>
              <td><input  type="number" name="txtQty<?php echo $index ?>" id="txtQty<?php echo $index ?>" value="<?php echo $row['Amount']; ?>" OnKeyUp="calculateAmount(<?php echo $index ?>)"></td>
              <td>
                <input class="theClassName" type="number" name="txtTotal<?php echo $index ?>" id="txtTotal<?php echo $index ?>" value="<?php echo $row['Total']; ?>" readonly>
              </td>
              <td align="center">
                <button class="deleteDep" value="Delete" onclick="myFunction(<?php echo $index ?>)">X
             </td>
            </tr>
            <?php
          }
          ?>
          </tbody>
        </table>
        <hr>
        <b class="">ข้อมูลรถ</b>
        <hr>
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>ลำดับ</th>
              <th>ทะเบียนรถ</th>
              <th>เลขตัวถัง</th>
              <th>เลขที่เอกสาร</th>
              <th>หมายเหตุ</th>
              <th>เพิ่มเติม</th>
            </tr>
            </thead>
            <tbody>
              <?php
                $index =0;
                  foreach ($select_car as $row) {
                      $index++;
                      ?>
              <tr>
                <td><?php echo $index; ?></td>
                <td><?php echo $row['license'] ?> <?php echo $row['province_license'] ?></td>
                <td><?php echo $row['body_number'] ?></td>
                <td><?php echo $row['id_document'] ?></td>
                <td><?php echo $row['remark'] ?></td>
                <td align="center">
                  <a class="btn btn-sm btn-primary text-white" onclick="DetailOnclick(<?php echo $row['id']; ?>)"><i class="far fa-file mr-1"></i>More Info </a>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        <hr>
        <b class="">รายละเอียดการใช้บริการ รายละเอียดการใช้บริการ</b>
        <hr>
         <a href="#checkcar" class="btn btn-primary" data-toggle="collapse" id="b1">ตรวจสภาพรถ <cl style="display:none;" id="cc"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#act" class="btn btn-secondary" data-toggle="collapse" id="b2">พรบ.<cl style="display:none;" id="cc2"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#tax" class="btn btn-success" data-toggle="collapse" id="b3">ภาษี<cl style="display:none;" id="cc3"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#insurance" class="btn btn-danger" data-toggle="collapse" id="b4">ประกันภาคสมัครใจ<cl style="display:none;" id="cc4"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#change" class="btn btn-warning" data-toggle="collapse" id="b5">เปลี่ยนทะเบียน<cl style="display:none;" id="cc5"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#transfer" class="btn btn-info" data-toggle="collapse" id="b6">โอน<cl style="display:none;" id="cc6"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#move" class="btn btn-light" data-toggle="collapse" id="b7">ย้ายทะเบียน<cl style="display:none;" id="cc7"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <br><small class=" text-muted">*กรุณาเลือกบริการเพื่อดูรายการใช้บริการ</small>
        <hr>
        <div id="checkcar" class="collapse">
          <b class="text-primary">รายละเอียดการใช้บริการ ตรวจสภาพรถ</b>
          <hr>
          <table id="dataTable2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>จังหวัด</th>
                <th>สถานะ</th>
                <th>หมายเหตุ</th>
                <th>ราคา</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_checkcar as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>
                  </td>
                  <td>                  
                      <?php echo $row['province_license']  ?>
                  </td>
                  <td><?php echo $row['ConditionResults']  ?></td>
                  <td><?php echo $row['Remark']  ?></td>
                  <td><?php echo $row['Price']  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_carcheck(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_carcheck(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          <hr>
        </div>
        <div id="act" class="collapse">
          <b class="text-secondary">รายละเอียดการใช้บริการ พรบ.</b>
          <hr>
          <table id="dataTable3" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>จังหวัด</th>
                <th>บริษัทที่คุ้มครอง</th>
                <th>ค่าบริการ</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_act as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>
                  </td>
                  <td>                  
                      <?php echo $row['province_license']  ?>
                  </td>
                  <td><?php echo $row['type']  ?></td>
                  <td><?php echo $row['Price']  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_act(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_act(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>

           <hr>
        </div>
        <div id="tax" class="collapse">
          <b class="text-success">รายละเอียดการใช้บริการ ภาษี</b>
          <hr>
          <table id="dataTable4" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>วันครบกำหนดครั้งต่อไป</th>
                <th>ค่าบริการ</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_tax as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>  <?php echo $row['province_license']  ?>
                  </td>
                  <td><?php echo convertDateThai($row['endDate'])  ?></td>
                  <td><?php echo $row['total']  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_tax(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_tax(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          <hr>
        </div>
        <div id="insurance" class="collapse">
          <b class="text-danger">รายละเอียดการใช้บริการ ประกันภัยภาคสมัครใจ</b>
          <hr>
          <table id="dataTable5" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>บริษัทที่คุ้มครอง</th>
                <th>เบี้ยประกัน</th>
                <th>ทุนประกัน</th>
                <th>วันสิ้นสุดการคุ้มครอง</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_insurance as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>  <?php echo $row['province_license']  ?>
                  </td>
                  <td><?php echo $row['type']  ?></td>
                  <td><?php echo $row['premium']  ?></td>
                  <td><?php echo $row['insurance']  ?></td>
                  <td><?php echo convertDateThai($row['endDate'])  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_insurance(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_insurance(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          <hr>
        </div>
        <div id="change" class="collapse">
          <b class="text-warning">รายละเอียดการใช้บริการ เปลี่ยนทะเบียน</b>
          <hr>
          <table id="dataTable6" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ เดิม</th>
                <th>ทะเบียนรถ ใหม่</th>
                <th>ค่าบริการ</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_change as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>  <?php echo $row['province_license']  ?>
                  </td>
                  <td >
                      <?php echo $row['license_new']  ?>  <?php echo $row['province_license_new']  ?>
                  </td>
                  <td><?php echo $row['qtyprice']  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_change(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_change(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>

          <hr>
        </div>
        <div id="transfer" class="collapse">
          <b class="text-info">รายละเอียดการใช้บริการ การโอน</b>
          <hr>
          <table id="dataTable7" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>ชื่อผู้รับ</th>
                <th>ค่าบริการ</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_transfer as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>  <?php echo $row['province_license']  ?>
                  </td>
                  <td >
                      <?php echo $row['RFName']  ?>  <?php echo $row['RLName']  ?>
                  </td>
                  <td><?php echo $row['qtyprice']  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_transfer(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_transfer(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          <hr>
        </div>
        <div id="move" class="collapse">
          <b class="">รายละเอียดการใช้บริการ การย้ายทะเบียน</b>
          <hr>
          <table id="dataTable8" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>ต้นทาง</th>
                <th>ปลายทาง</th>
                <th>ค่าบริการ</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_move as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>  <?php echo $row['province_license']  ?>
                  </td>
                  <td >
                      <?php echo $row['source']  ?> 
                  </td>
                  <td><?php echo $row['destination']  ?></td>
                  <td><?php echo $row['qtyprice']  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_transfer(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_transfer(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
        </div>
        <div class="form-group mt-3">
             <center>
               <button type="reset" class="btn btn-default float-right" onclick="cancelOnclick()">กลับ</button>
             </center>
           </div>

        <!-- /.card -->
      </div><!-- /.container-fluid -->
      </form>
    </section>
    <!-- /.content -->
  </div>

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->

 <!-- jQuery -->
 <script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>  
<!-- address -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dependencies/JQL.min.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>


<script>

  function DetailOnclick(id) {
    window.location = '../car/detail.php?id=' + id;
  }

  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable3').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable4').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable5').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable6').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable7').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable8').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });

  function EditOnclick_act(id) {
    window.location = '../act/detail.php?id=' + id;
  }

  function receiptPrint_act(id) {
    window.open("../act/receipt.php?id=" + id);
  }

  function EditOnclick_carcheck(id) {
    window.location = '../carcheck/detail.php?id=' + id;
  }

  function receiptPrint_carcheck(id) {
    window.open("../carcheck/receipt.php?id=" + id);
  }

  function EditOnclick_change(id) {
    window.location = '../change/detail.php?id=' + id;
  }

  function receiptPrint_change(id) {
    window.open("../change/receipt.php?id=" + id);
  }

  function EditOnclick_tax(id) {
    window.location = '../tax/detail.php?id=' + id;
  }

  function receiptPrint_tax(id) {
    window.open("../tax/receipt.php?id=" + id);
  }

  function EditOnclick_insurance(id) {
    window.location = '../insurance/detail.php?id=' + id;
  }

  function receiptPrint_insurance(id) {
    window.open("../insurance/receipt.php?id=" + id);
  }

  function EditOnclick_transfer(id) {
    window.location = '../transfer/detail.php?id=' + id;
  }

  function receiptPrint_transfer(id) {
    window.open("../transfer/receipt.php?id=" + id);
  }

  function EditOnclick(id) {
    window.location = '../car/detail.php?id=' + id;
  }

  function DelOnclick(id) {
    if(confirm('คุณต้องการลบข้อมูล ใช่ หรือ ไม่?') == true)
       window.location = '../car/carInfo.php?id=' + id +'&__cmd=delete';
  }

$(document).ready(function(){
    $("#b1").click(function(){
        $("#checkcar").show();
        $("#act").hide();
        $("#tax").hide();
        $("#insurance").hide();
        $("#change").hide();
        $("#transfer").hide();
        $("#move").hide();
        $("#cc").show();
        $("#cc2").hide();
        $("#cc3").hide();
        $("#cc4").hide();
        $("#cc5").hide();
        $("#cc6").hide();
        $("#cc7").hide();
    });

    $("#b2").click(function(){
       $("#checkcar").hide();
        $("#act").show();
        $("#tax").hide();
        $("#insurance").hide();
        $("#change").hide();
        $("#transfer").hide();
        $("#move").hide();
        $("#cc2").show();
        $("#cc3").hide();
        $("#cc4").hide();
        $("#cc5").hide();
        $("#cc6").hide();
        $("#cc7").hide();
        $("#cc").hide();
    });

    $("#b3").click(function(){
       $("#checkcar").hide();
        $("#act").hide();
        $("#tax").show();
        $("#insurance").hide();
        $("#change").hide();
        $("#transfer").hide();
        $("#move").hide();
        $("#cc3").show();
        $("#cc4").hide();
        $("#cc5").hide();
        $("#cc6").hide();
        $("#cc7").hide();
        $("#cc2").hide();
        $("#cc").hide();
    });

    $("#b4").click(function(){
        $("#checkcar").hide();
        $("#act").hide();
        $("#tax").hide();
        $("#insurance").show();
        $("#change").hide();
        $("#transfer").hide();
        $("#move").hide(); 
        $("#cc4").show();
        $("#cc5").hide();
        $("#cc6").hide();
        $("#cc7").hide();
        $("#cc3").hide();
        $("#cc2").hide();
        $("#cc").hide();
    });

    $("#b5").click(function(){
      $("#checkcar").hide();
        $("#act").hide();
        $("#tax").hide();
        $("#insurance").hide();
        $("#change").show();
        $("#transfer").hide();
        $("#move").hide();
       $("#cc5").show();
       $("#cc6").hide();
        $("#cc7").hide();
        $("#cc4").hide();
        $("#cc3").hide();
        $("#cc2").hide();
        $("#cc").hide();
    });

    $("#b6").click(function(){
      $("#checkcar").hide();
        $("#act").hide();
        $("#tax").hide();
        $("#insurance").hide();
        $("#change").hide();
        $("#transfer").show();
        $("#move").hide();
        $("#cc6").show();
        $("#cc7").hide();
        $("#cc5").hide();
        $("#cc4").hide();
        $("#cc3").hide();
        $("#cc2").hide();
        $("#cc").hide();
    });

    $("#b7").click(function(){
        $("#checkcar").hide();
        $("#act").hide();
        $("#tax").hide();
        $("#insurance").hide();
        $("#change").hide();
        $("#transfer").hide();
        $("#move").show();
        $("#cc7").show();
        $("#cc6").hide();
        $("#cc5").hide();
        $("#cc4").hide();
        $("#cc3").hide();
        $("#cc2").hide();
        $("#cc").hide();
    });
});


</script>

<script>
   (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-33058582-1', 'auto', {
            'name': 'Main'
        });
        ga('Main.send', 'event', 'jquery.Thailand.js', 'GitHub', 'Visit');


        $.Thailand({
            database: '../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/database/db.json', 

            $district: $('#demo1 [name="district"]'),
            $amphoe: $('#demo1 [name="amphoe"]'),
            $province: $('#demo1 [name="province"]'),
            $zipcode: $('#demo1 [name="zipcode"]'),

            onDataFill: function(data){
                console.info('Data Filled', data);
            },

            onLoad: function(){
                console.info('Autocomplete is ready!');
                $('#loader, .demo').toggle();
            }
        });

        // watch on change

        $('#demo1 [name="district"]').change(function(){
            console.log('ตำบล', this.value);
        });
        $('#demo1 [name="amphoe"]').change(function(){
            console.log('อำเภอ', this.value);
        });
        $('#demo1 [name="province"]').change(function(){
            console.log('จังหวัด', this.value);
        });
        $('#demo1 [name="zipcode"]').change(function(){
            console.log('รหัสไปรษณีย์', this.value);
        });

function cancelOnclick() {
 window.location = 'customerlist.php';
}

</script>

</body>
</html>
