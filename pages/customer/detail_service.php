<?php 
    require_once('../../php/connect.php');
    $sql = "SELECT * FROM orders WHERE OrderID = '".$_GET['OrderID']."'";
    $result_sevice = $conn->query($sql) or die($conn->error);
    if ($result_sevice->num_rows > 0){
        $row = $result_sevice->fetch_assoc();
    } else {
        header('Location: ../customer');
    }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Customer Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
<!-- Navbar & Main Sidebar Container -->
<?php include_once('../includes/sidebar.php') ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Customer</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="../customer">ข้อมูลลูกค้า</a></li>
              <li class="breadcrumb-item active">รายละเอียดลูกค้า</li>
            </ol>
          </div>
        </div>
      </div>
    <!-- /.container-fluid -->

    <section class="content">
        <div class="card">
            <!-- Table row -->
            <div class="row">
                <div class="col-12">
                    <div class="card-header">
                        <h3 class="card-title d-inline-block">ประวัติ</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                    <table id="dataTable" class="table table-bordered ">
                        <tbody>
                            <tr>
                                <th width="12%" rowspan="10"  class="bg-gray-light">
                                วันที่ <?php echo date_format(new DateTime($row['OrderDate']),"d/m/Y"); ?>
                                </th>
                                <th width="13%">
                                ตรวจสภาพรถ
                                </th>
                                <td>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="ราคา" value="<?php echo $row['checkcar_price'];?>" >
                                    <div class="input-group-append">
                                        <span class="input-group-text">บาท</span>
                                    </div>
                                </div>
                                </td>
                                <td colspan="2">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="ผลตรวจ">
                                    <div class="input-group-append">
                                        <span class="fa fa-edit input-group-text"></span>
                                    </div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                พรบ.
                                </th>
                                <td>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="ราคา" value="<?php echo $row['porobor_price'];?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text">บาท</span>
                                    </div>
                                </div>                              
                                </td>
                                <td>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="บริษัทประกัน">
                                    <div class="input-group-append">
                                        <span class="fa fa-building input-group-text"></span>
                                    </div>
                                </div>
                                </td>
                                <td>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="วันที่">
                                    <div class="input-group-append">
                                        <span class="fa fa-calendar-alt input-group-text"></span>
                                    </div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                ภาษี
                                </th>
                                <td>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="ราคา" value="<?php echo $row['pasee_price'];?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text">บาท</span>
                                    </div>
                                </div>                              
                                </td>
                                <td colspan="2">
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="วันที่">
                                    <div class="input-group-append">
                                        <span class="fa fa-calendar-alt input-group-text"></span>
                                    </div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                ประกันภาคสมัครใจ
                                </th>
                                <td>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="ราคา" value="<?php echo $row['insurance_price'];?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text">บาท</span>
                                    </div>
                                </div>
                                </td>
                                <td>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="ประกันชั้น">
                                    <div class="input-group-append">
                                        <span class="fa fa-clipboard input-group-text"></span>
                                    </div>
                                </div>
                                </td>
                                <td>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="บริษัทประกัน">
                                    <div class="input-group-append">
                                        <span class="fa fa-building input-group-text"></span>
                                    </div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="3">
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="วันที่">
                                    <div class="input-group-append">
                                        <span class="fa fa-calendar-alt input-group-text"></span>
                                    </div>
                                </div>
                                </td>
                            </tr>
                            <t>
                                <th>
                                ตรวจแก๊ส
                                </th>
                                <td>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="บริษัท">
                                    <div class="input-group-append">
                                        <span class="fa fa-building input-group-text"></span>
                                    </div>
                                </div>
                                </td>
                                <td colspan="2">
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="วันที่">
                                    <div class="input-group-append">
                                        <span class="fa fa-calendar-alt input-group-text"></span>
                                    </div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right">
                                <div class="input-group col-6">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        รวมเป็นเงินทั้งหมด 
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" value="<?php echo $row['Total'];?>">
                                    <div class="input-group-append">
                                    <span class="input-group-text">บาท</span>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr class="bg-light">
                                <td colspan="4">
                                <b>ผู้บันทึก</b> นายชาย สมใจ
                                </td>
                            </tr>
                            <tr class="bg-light">
                                <td colspan="2">
                                <b>ผู้ติดตาม</b> พบใจ
                                </td>
                                <td colspan="2">
                                <b>ประเภท</b> จดหมาย
                                </td>
                            </tr>
                            <tr class="bg-light">
                                <td colspan="2">
                                <b>หมายเหตุ</b> ลุงพลแนะนำ
                                </td>
                                <td colspan="2">
                                <a href="#">
                                    <button type="button" class="btn btn-primary btn-xm mr-2">
                                    <i class="fa fa-edit"></i> แก้ไข
                                    </button>
                                </a>
                                <a target ="_blank" href="invoice-print.php?OrderID=<?php echo $row['OrderID'] ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                                <a href="profile_customer.php?id=<?php echo $row['UserID'] ?>">
                                    <button type="button" class="btn btn-danger btn-xm mr-2">
                                    กลับ
                                    </button>
                                </a> 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </section>
                    
  </div>
  <!-- /.content-wrapper -->
  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>
  
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- daterangepicker -->
<script src="../../plugins/moment/min/moment.min.js"></script>
<script src="../../plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/locales/js/bootstrap-datepicker.th.js"></script>
<script src="../../plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
</body>
</html>
