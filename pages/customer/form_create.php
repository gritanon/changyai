<?php

session_start();
require_once "../../ConnectDatabase/connectionDb.inc.php";

$id = getIsset("id");
$cmd = getIsset("__cmd");

if (intval($id) > 0){ 
  //Update 
  if ($cmd == 'save') {
    $value = array(
        "FName"=>getIsset('FName')
         ,"LName"=>getIsset('LName')
         ,"Address"=>getIsset('Address')
         ,"district"=>getIsset('district')
         ,"amphoe"=>getIsset('amphoe')
         ,"province"=>getIsset('province')
         ,"zipcode"=>getIsset('zipcode')
         ,"Tel"=>getIsset('Tel')
    );

    if ($conn->update("customer", $value, array("id" => $id))) {
        alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','index.php');
    }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
    }

  }else{

    $tbl_cus = $conn->select('customer', array('id' => getIsset('id')), true);

    if($tbl_cus != null){
      $FName = $tbl_cus["FName"];
      $LName = $tbl_cus["LName"];
      $Address = $tbl_cus["Address"];
      $district = $tbl_cus["district"];
      $amphoe = $tbl_cus["amphoe"];
      $province = $tbl_cus["province"];
      $zipcode = $tbl_cus["zipcode"];
      $Tel = $tbl_cus["Tel"];
    }
  }
}else{
  //Insert
  if ($cmd == 'save') {

    $value = array(
      "FName"=>getIsset('FName')
       ,"LName"=>getIsset('LName')
       ,"Address"=>getIsset('Address')
       ,"district"=>getIsset('district')
       ,"amphoe"=>getIsset('amphoe')
       ,"province"=>getIsset('province')
       ,"zipcode"=>getIsset('zipcode')
       ,"Tel"=>getIsset('Tel')
    );
     if($conn->create("customer",$value)){
      $cusID = $conn->getLastInsertId();
      alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','../car/carInfos.php?cusID='.$cusID);
     }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
     }

  }
}

if($cmd == 'delete'){
  //delete
    if($conn->delete("customer",array("id"=>$id))){
        alertMassageAndRedirect('ลบสำเร็จ','index.php');
    }else{
        alertMassage("ลบไม่สำเร็จ");
    }
}


?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
  <!-- address -->
  <link rel="stylesheet" href="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dist/jquery.Thailand.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/check_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ข้อมูลลูกค้า</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../customer">ข้อมูลลูกค้า</a></li>
              <li class="breadcrumb-item active">เพิ่มข้อมูลลูกค้า</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <form name="frmMain" id="demo1" class="demo" style="display:none;" OnSubmit="return chkString();" autocomplete="off" uk-grid >
      <div class="container-fluid">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">เพิ่มข้อมูลลูกค้า</h3>
          </div>
          <div class="col-12 table-responsive card-body">
            <b class="">รายละเอียดข้อมูลลูกค้า</b>
            <hr>
            <div class="card-body">
              <input type="hidden" name="id" value="<?php echo $id ?>">
              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" >ชื่อ</label>
                <div class="col-10">
                  <input type="text" class="form-control" placeholder="ชื่อ" name="FName" value="<?php echo $FName; ?>">
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" >นามสกุล</label>
                <div class="col-10">
                  <input type="text" class="form-control" placeholder="นามสกุล" name="LName" value="<?php echo $LName; ?>">
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" >เบอร์โทร</label>
                <div class="col-10">
                  <input type="number" class="form-control" placeholder="ใส่ตัวเลข 10 ตัวเท่านั้น เช่น 0812345678" name="Tel" value="<?php echo $Tel; ?>" maxlength="10">
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" >ที่อยู่</label>
                <div class="col-10">
                  <input type="text" class="form-control" placeholder="บ้านเลขที่" name="Address" value="<?php echo $Address; ?>">
                </div>
              </div>
              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" ></label>
                <div class="col-5">
                  <input type="text" class="form-control" placeholder="ตำบล / แขวง" name="district" value="<?php echo $district; ?>">
                </div>
                <div class="col-5 ">
                  <input type="text" class="form-control" placeholder="อำเภอ / เขต" name="amphoe" value="<?php echo $amphoe; ?>">
                </div>
              </div>
              <div class="row form-group">
              <label class="ml-5 mr-10 mt-1 col-sm-1" ></label>
                <div class="col-5 ">
                  <input type="text" class="form-control" placeholder="จังหวัด" name="province" value="<?php echo $province; ?>">
                </div>
                <div class="col-5">
                  <input type="number" class="form-control" placeholder="รหัสไปรษณีย์" name="zipcode" value="<?php echo $zipcode; ?>">
                </div>
              </div>

            <?php

                        $index =0;
                                foreach ($tbl as $row) {
                                    $index++;
                                    $SumTotal = $SumTotal + $row['Total'];
                                    ?>
          <tr>
              <td><input type="text" name="txtName<?php echo $index ?>" value="<?php echo $row['Description']; ?>"></td>
              <td><input  type="number" name="txtPrice<?php echo $index ?>" id="txtPrice<?php echo $index ?>" value="<?php echo $row['Count']; ?>" OnKeyUp="calculateAmount(<?php echo $index ?>)"></td>
              <td><input  type="number" name="txtQty<?php echo $index ?>" id="txtQty<?php echo $index ?>" value="<?php echo $row['Amount']; ?>" OnKeyUp="calculateAmount(<?php echo $index ?>)"></td>
              <td>
                <input class="theClassName" type="number" name="txtTotal<?php echo $index ?>" id="txtTotal<?php echo $index ?>" value="<?php echo $row['Total']; ?>" readonly>
              </td>
              <td align="center">
                <button class="deleteDep" value="Delete" onclick="myFunction(<?php echo $index ?>)">X
             </td>
            </tr>
            <?php
          }
          ?>
          </tbody>
        </table>


        <div class="form-group">
             <center>
               <button type="submit" class="btn btn-primary" name="__cmd" value="save">ตกลง</button>
               <button type="reset" class="btn btn-default" onclick="cancelOnclick()">ยกเลิก</button>
             </center>
           </div>

        <!-- /.card -->
      </div><!-- /.container-fluid -->
      </form>
    </section>
    <!-- /.content -->
  </div>

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->

 <!-- jQuery -->
 <script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>  
<!-- address -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dependencies/JQL.min.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>


<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

<script>
   (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-33058582-1', 'auto', {
            'name': 'Main'
        });
        ga('Main.send', 'event', 'jquery.Thailand.js', 'GitHub', 'Visit');


        $.Thailand({
            database: '../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/database/db.json', 

            $district: $('#demo1 [name="district"]'),
            $amphoe: $('#demo1 [name="amphoe"]'),
            $province: $('#demo1 [name="province"]'),
            $zipcode: $('#demo1 [name="zipcode"]'),

            onDataFill: function(data){
                console.info('Data Filled', data);
            },

            onLoad: function(){
                console.info('Autocomplete is ready!');
                $('#loader, .demo').toggle();
            }
        });

        // watch on change

        $('#demo1 [name="district"]').change(function(){
            console.log('ตำบล', this.value);
        });
        $('#demo1 [name="amphoe"]').change(function(){
            console.log('อำเภอ', this.value);
        });
        $('#demo1 [name="province"]').change(function(){
            console.log('จังหวัด', this.value);
        });
        $('#demo1 [name="zipcode"]').change(function(){
            console.log('รหัสไปรษณีย์', this.value);
        });

function cancelOnclick() {
    window.location = 'index.php';
}

function chkString()
{

    if(document.frmMain.FName.value.length  == "")
    {
    alert('กรุณากรอกชื่อ');
    return false;
    }

    if(document.frmMain.LName.value.length  == "")
    {
    alert('กรุณากรอกนามสกุล');
    return false;
    }

    if(document.frmMain.Tel.value.length < 10 || document.frmMain.Tel.value.length > 10)
    {
    alert('กรุณาใส่เบอร์โทรศัพท์ให้ถูกต้อง');
    return false;
    }

    if(document.frmMain.Tel.value.length  == "")
    {
    alert('กรุณากรอกเบอร์โทร');
    return false;
    }

    if(document.frmMain.Address.value.length  == "")
    {
    alert('กรุณากรอกที่อยู่');
    return false;
    }

    if(document.frmMain.district.value.length  == "")
    {
    alert('กรุณากรอกตำบล');
    return false;
    }

    if(document.frmMain.amphoe.value.length  == "")
    {
    alert('กรุณากรอกอำเภอ');
    return false;
    }

    if(document.frmMain.province.value.length  == "")
    {
    alert('กรุณากรอกจังหวัด');
    return false;
    }

    if(document.frmMain.zipcode.value.length  == "")
    {
    alert('กรุณากรอกรหัสไปรษณีย์');
    return false;
    }

}
</script>

</body>
</html>
