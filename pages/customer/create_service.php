<?php 
    require_once('../../php/connect.php');
    $sql = "SELECT * FROM customer WHERE id = '".$_GET['id']."'";
    $result = $conn->query($sql) or die($conn->error);

    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
    } else {
        header('Location: ../customer');
    }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Customer Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
<!-- Navbar & Main Sidebar Container -->
<?php include_once('../includes/sidebar.php') ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="../customer">ข้อมูลลูกค้า</a></li>
              <li class="breadcrumb-item active">เพิ่มข้อมูลการตรวจ</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    ตรอ. ช่างใหญ่ เซอร์วิส
                    <small class="float-right">วันที่ <?php echo date_format(new DateTime($row['created_at']),"d/m/Y");  ?></small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    ทะเบียนรถ <?php echo $row['car'] ?><br>
                    เจ้าของรถ toyota******<br>
                    ชื่อ <?php echo $row['name'] ?><br>
                    เบอร์โทร <?php echo $row['phone'] ?><br>
                    วันที่จดทะเบียน <?php echo date_format(new DateTime($row['registration']),"d/m/Y");  ?>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    จังหวัด <?php echo $row['province'] ?><br>
                    รุ่น MTX****<br>
                    ที่อยู่ <?php echo $row['address'] ?><br>
                    ชนิดเชื้อเพลิง ___________<br>
                    วันที่เสียภาษี __________
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    รย. <?php echo $row['type'] ?><br>
                    หมายเลขตัวถัง <?php echo $row['address'] ?><br>
                    หมายเลขเครื่อง ___________<br>
                    หมายเลขถังแก๊ส ___________<br>
                    วัน พรบ. หมด <?php echo date_format(new DateTime($row['act']),"d/m/Y");  ?>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row mt-3">
                <div class="col-12 table-responsive">
                  <form action="save_checkout.php" method="post" name="form1">
                  <table class="table table-bordered">
                    <thead>
                    <tr>
                      <th>เพิ่มข้อมูลการตรวจ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td colspan="2">
                        <div class="row">
                          <div class="col-12">
                          <table class="table table-bordered table-hover m-auto">
                            <tr>
                              <th>
                              <label class="radio-inline">
                                <input type="checkbox" id="checkcar" onclick="chk_value_checkcar()" >
                              </label>
                              <div class="col-md-8 margin-bottom-15">
                              </th>
                              <th width="13%">
                                ตรวจสภาพรถ
                              </th>
                              <td colspan="3">
                                <fieldset id="checkcar1">
                                    <div class="input-group">
                                        <input id="txtVol1_1" type="hidden" value="0"> 
                                        <input id="txtVol2_1" type="text" name="checkcar_price" value="0" class="form-control col-6" placeholder="ราคา"> 
                                        <input id="txtVol3_1" type="hidden">
                                        <div class="input-group-append">
                                            <span class="input-group-text">บาท</span>
                                        </div>

                                        <select type="text" class="form-control col-6 ml-2">
                                          <option value="lamphun">ผลตรวจ</option>
                                          <option value="chaingmai">ผ่าน</option>
                                          <option value="bankok">ไม่ผ่าน</option>
                                        </select>
                                        <div class="input-group-append">
                                            <span class="fa fa-edit input-group-text"></span>
                                        </div>
                                    </div>
                                    <div class="input-group mt-2">
                                      <textarea class="form-control" name="additional" rows="3" placeholder="หมายเหตุ"></textarea>
                                    </div>
                                </fieldset>       
                              </td>
                            </tr>
                            <tr>
                              <th>
                                <label>
                                   <input type="checkbox" id="porobor" onclick="chk_value_porobor()" >
                                </label>
                              </th>
                              <th>
                                พรบ.
                              </th>
                              <td colspan="3">
                                <fieldset id="porobor1" disabled>
                                  <div class="input-group">
                                      <input id="txtVol1_2" type="hidden" value="0"> 
                                      <input id="txtVol2_2" type="text" name="porobor_price" value="0" class="form-control" placeholder="ราคา"> 
                                      <input id="txtVol3_2" type="hidden">
                                      <div class="input-group-append">
                                          <span class="input-group-text">บาท</span>
                                      </div>

                                    <input type="text" class="form-control ml-2" placeholder="บริษัทประกัน">
                                    <div class="input-group-append">
                                        <span class="fa fa-building input-group-text"></span>
                                    </div>

                                    <input type="text" class="form-control datepicker ml-2" data-date-format="mm/dd/yyyy" placeholder="วันที่" >
                                    <div class="input-group-append">
                                        <span class="fa fa-calendar-alt input-group-text"></span>
                                    </div>
                                 </div>
                                </fieldset>  
                              </td>
                            </tr>
                            <tr>
                              <th>
                                <label>
                                   <input type="checkbox" id="pasee" onclick="chk_value_pasee()" >
                                </label>
                              </th>
                              <th>
                                ภาษี
                              </th>
                              <td colspan="3">
                                <fieldset id="pasee1" disabled>
                                    <div class="input-group">
                                        <input id="txtVol1_3" type="hidden" value="0"> 
                                        <input id="txtVol2_3" type="text" value="0" name="pasee_price" class="form-control" placeholder="ราคา"> 
                                        <input id="txtVol3_3" type="hidden">
                                        <div class="input-group-append">
                                            <span class="input-group-text">บาท</span>
                                        </div>

                                        <input type="text" class="form-control datepicker ml-2" data-date-format="mm/dd/yyyy" placeholder="วันที่" >
                                        <div class="input-group-append">
                                            <span class="fa fa-calendar-alt input-group-text"></span>
                                        </div>
                                    </div>
                                </fieldset>  
                              </td>
                            </tr>
                            <tr>
                              <th>
                                <label>
                                   <input type="checkbox" id="insurance" onclick="chk_value_insurance()" >
                                </label>
                              </th>
                              <th>
                                ประกันภาคสมัครใจ
                              </th>
                              <td colspan="2">
                                <fieldset id="insurance1" disabled>
                                    <div class="input-group">
                                        <input id="txtVol1_4" type="hidden" value="0"> 
                                        <input id="txtVol2_4" type="text" value="0"  name="insurance_price" class="form-control" placeholder="ราคา"> 
                                        <input id="txtVol3_4" type="hidden">
                                        <div class="input-group-append">
                                            <span class="input-group-text">บาท</span>
                                        </div>

                                        <input type="text" class="form-control ml-2" placeholder="ประกันชั้น">
                                        <div class="input-group-append">
                                            <span class="fa fa-clipboard input-group-text"></span>
                                        </div>
                                        <br>
                                        <input type="text" class="form-control ml-2" placeholder="บริษัทประกัน">
                                        <div class="input-group-append">
                                            <span class="fa fa-building input-group-text"></span>
                                        </div>

                                        <input type="text" class="form-control datepicker ml-2" data-date-format="mm/dd/yyyy" placeholder="วันที่">
                                        <div class="input-group-append">
                                            <span class="fa fa-calendar-alt input-group-text"></span>
                                        </div>
                                    </div>
                                </fieldset>
                              </td>
                            </tr>
                            <tr>
                              <th>
                                <label>
                                  <input type="checkbox" id="checkgas" onclick="chk_value_checkgas()" >
                                </label>
                              </th>
                              <th>
                                ตรวจแก๊ส
                              </th>
                              <td colspan="2">
                                <fieldset id="checkgas1" disabled>
                                <div class="input-group ">
                                  <input id="txtVol1_5" type="hidden" value="0"> 
                                  <input id="txtVol2_5" type="text" value="0"  name="checkgas_price" class="form-control" placeholder="ราคา"> 
                                  <input id="txtVol3_5" type="hidden">
                                  <div class="input-group-append">
                                      <span class="input-group-text">บาท</span>
                                  </div>
                                  <input type="text" class="form-control ml-2" placeholder="บริษัท">
                                  <div class="input-group-append">
                                      <span class="fa fa-building input-group-text"></span>
                                  </div>
  
                                  <input type="text" class="form-control datepicker ml-2" data-date-format="mm/dd/yyyy" placeholder="วันที่">
                                  <div class="input-group-append">
                                      <span class="fa fa-calendar-alt input-group-text"></span>
                                  </div>
                                </div>
                                </fieldset>
                              </td>
                            </tr>
                            <tr class="bg-light">
                              <td colspan="4">
                                <div class="input-group col-9 float-right">
                                  <b class="mt-2 mr-2">รวมเป็นเงินทั้งหมด</b>
                                  <input type="hidden" name="hdnLine" value="6">
                                  <input type="text" class="form-control" placeholder="ราคา" id="txtSum" name="total" readonly>
                                  <div class="input-group-append">
                                      <span class="input-group-text">บาท</span>
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="4">
                                <div class="input-group">
                                  <input type="text" class="form-control" placeholder="ผู้บันทึก">
                                  <div class="input-group-append">
                                      <span class="fa fa-user input-group-text"></span>
                                  </div>

                                  <input type="text" class="form-control ml-2" placeholder="ผู้ติดตาม">
                                  <div class="input-group-append">
                                      <span class="fa fa-user input-group-text"></span>
                                  </div>
                                  
                                  <input type="text" class="form-control ml-2" placeholder="ประเภทการติดตาม">
                                  <div class="input-group-append">
                                      <span class="fa fa-paper-plane input-group-text"></span>
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr class="bg-light">
                              <td colspan="5">
                                <div class="form-group">
                                    <textarea class="form-control" name="additional" rows="3" placeholder="หมายเหตุ"></textarea>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="5">
                                <div class="card-footer">
                                    <input type="hidden" name="id" value="<?php echo $row["id"];?>">
                                    <input onclick="enableBtn();JavaScript:fncCal();" class="btn btn-primary" value="คำนวน">
                                    <button type="submit" class="btn btn-primary" id="myBtn" disabled="disabled">เพิ่ม</button>
                                    <a href="index.php" class="btn btn-danger float-right">
                                    กลับ
                                    </a> 
                                </div>
                              </td>
                            </tr>
                          </table>
                        </div>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                  </form>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>
  
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- daterangepicker -->
<script src="../../plugins/moment/min/moment.min.js"></script>
<script src="../../plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
<script src="../../plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>
<!--checkbox-->

<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

<script>
  $(document).ready(function () {
      $('.datepicker').datepicker({
          format: 'dd/mm/yyyy',
          todayBtn: true,
          language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
          thaiyear: true              //Set เป็นปี พ.ศ.
      }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน
  });
</script>

<!-- Check box เลือกรายการ -->
<script>
  function chk_value_checkcar(){
    var enabled=document.getElementById('checkcar').checked;
    $(element).is(":checkcar1"); 
    document.getElementById('checkcar2').disabled=!enabled;
  }

  function chk_value_porobor(){
    var enabled=document.getElementById('porobor').checked;
    document.getElementById('porobor1').disabled=!enabled;
    document.getElementById('porobor2').disabled=!enabled;
    document.getElementById('porobor3').disabled=!enabled;
  }

  function chk_value_pasee(){
    var enabled=document.getElementById('pasee').checked;
    document.getElementById('pasee1').disabled=!enabled;
    document.getElementById('pasee2').disabled=!enabled;
  }

  function chk_value_insurance(){
    var enabled=document.getElementById('insurance').checked;
    document.getElementById('insurance1').disabled=!enabled;
    document.getElementById('insurance2').disabled=!enabled;
    document.getElementById('insurance3').disabled=!enabled;
    document.getElementById('insurance4').disabled=!enabled;
  }

  function chk_value_checkgas(){
    var enabled=document.getElementById('checkgas').checked;
    document.getElementById('checkgas1').disabled=!enabled;
  }

</script>

<!-- คำนวนก่อน แสดงปุ่ม submit -->
<script>
  function enableBtn() {
      document.getElementById("myBtn").disabled = false;
  }
</script>

<!-- คำนวนค่าใช้จ่าย -->
<script>
function fncCal()
{
	var tot = 0;
	var sum = 0;
	for(i=1;i<=document.form1.hdnLine.value;i++)
	{
		tot = parseInt(eval("document.form1.txtVol1_"+i+".value")) + parseInt(eval("document.form1.txtVol2_"+i+".value"))
		eval("document.form1.txtVol3_"+i+".value="+tot);
		sum = tot + sum;
		document.form1.txtSum.value=sum;
	}
}
</script>
</body>
</html>
