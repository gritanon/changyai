<?php
    session_start();
    require_once "../../ConnectDatabase/connectionDb.inc.php";

    $sql = "SELECT t.id,t.Date,t.RFName,t.RLName,t.RTel,t.source,t.province_license_new,t.license_new,t.province_license_old,t.license_old,t.destination,c.license,c.province_license,cm.FName , cm.LName , cm.Tel
            FROM move t inner join car c on t.carID = c.id
            inner join customer cm on t.cusID = cm.id ORDER BY t.id DESC";
    $select_all = $conn->queryRaw($sql);
    $total = sizeof($select_all);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/check_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../dashboard">ข้อมูลลูกค้า</a></li>
              <li class="breadcrumb-item moveive">ข้อมูลลูกค้า</li>
            </ol> -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title d-inline-block">ข้อมูลการย้าย</h3>
          <a href="moveInfo.php" class="btn btn-primary float-right "> + เพิ่มข้อมูลการย้าย</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive">
          <form action="moveListSearch.php" method="post">
              <div class="col-md-12 my-2 collapse" id="sdate">
                  <div class="input-group">
                      <text class="m-1 mr-3">ตั้งแต่วันที่</text>
                      <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="fa fa-calendar"></i>
                      </span>
                      </div>
                      <input type="text" name="start_date" class="form-control datepicker col-2 text-center" data-date-format="ํd/m/Y">
                      <text class="m-1 mx-3">ถึงวันที่</text>
                      <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="fa fa-calendar"></i>
                      </span>
                      </div>
                      <input type="text" name="end_date" class="form-control datepicker col-2 text-center" data-date-format="d/m/Y">
                      <input type="submit" value="ค้นหา">
                  </div>
              </div>
          </form>
          <div class="col-md-12 my-1" >
            <div class="row">
              <div class="col-12">
                <a href="#sdate"  data-toggle="collapse" class="float-right text-info mr-2"> ค้นหาจากวันที่</a>
              </div>
            </div>
          </div>
          <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>ลำดับ</th>
              <th>ชื่อ-นามสกุล</th>
              <th>ต้นทาง</th>
              <th>ปลายทาง</th>
              <th>ทะเบียนรถเก่า</th>
              <th>ทะเบียนรถใหม่</th>
              <th>วันที่ทำรายการ</th>
              <th>เพิ่มเติม</th>
              <th>ใบเสร็จรับเงิน</th>
              <th>แก้ไข</th>
              <th>ลบ</th>
            </tr>
            </thead>
            <tbody>
              <?php
                $index =0;
                  foreach ($select_all as $row) {
                      $index++;
                      ?>
              <tr>
                <td><?php echo $index; ?></td>
                <td><?php echo $row['FName']  ?> <?php echo $row['LName'] ?></td>
                <td><?php echo $row['source'] ?></td>
                <td><?php echo $row['destination'] ?></td>
                <td><?php echo $row['license_old'] ?> <?php echo $row['province_license_old'] ?></td>
                <td><?php echo $row['license_new'] ?> <?php echo $row['province_license_new'] ?></td>
                <td><?php echo convertDateThai($row['Date']) ?></td>
                <td align="center">
                  <a class="btn btn-sm btn-primary text-white" onclick="DetailOnclick(<?php echo $row['id']; ?>)"><i class="far fa-file mr-1"></i>More Info </a>
                </td>
                <td align="center">
                  <a class="btn btn-sm btn-success text-white" onclick="receiptPrint(<?php echo $row['id']; ?>)"><i class="fas fa-print mr-1"></i> Print</a>
                </td>
                <td align="center">
                  <a onclick="EditOnclick(<?php echo $row['id']; ?>)" class="btn btn-sm btn-warning text-white"><i class="fas fa-edit"></i>Edit</a>
                </td>
                <td align="center">
                  <a onclick="DelOnclick(<?php echo $row['id']; ?>)"  class="btn btn-sm btn-danger text-white"><i class="fas fa-trash-alt mr-1"> </i>Delete</a>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>  
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
<script src="../../plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>


<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
            }
    });
  });

  function DetailOnclick(id) {
    window.location = 'detail.php?id=' + id;
  }

  function EditOnclick(id) {
    window.location = 'moveInfo.php?id=' + id;
  }

  function DelOnclick(id) {
    if(confirm('คุณต้องการลบข้อมูล ใช่ หรือ ไม่?') == true)
       window.location = 'moveInfo.php?id=' + id +'&__cmd=delete';
  }

  function receiptPrint(id) {
    window.open("receipt.php?id=" + id);
  }

  $(document).ready(function () {
      $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
          language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
          thaiyear: true                //Set เป็นปี พ.ศ.
      }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน
  });

</script>

</body>
</html>
