<?php

session_start();
require_once "../../ConnectDatabase/connectionDb.inc.php";

$id = getIsset("id");
$cmd = getIsset("__cmd");

$sql = "SELECT c.id,cus.FName ,cus.LName,cus.address,cus.province,cus.district,cus.amphoe,cus.zipcode,cus.tel,c.license,c.registration,c.typecar,c.brand,c.generation,c.serial_number,c.gas_number,c.fuel_type,
c.province_license,c.body_number,cus.id as cusID,c.capacity from car c inner join customer cus on c.cusID = cus.id";
$select_all = $conn->queryRaw($sql);

$date = date('Y-m-d', strtotime('+3 months'));

if (intval($id) > 0){
  //Update
  if ($cmd == 'save') {
    $value = array(
      "cusID"=>getIsset('cusID')
       ,"carID"=>getIsset('carID')
       ,"ConditionResults"=>getIsset('ConditionResults')
       ,"Remark"=>getIsset('Remark')
      ,"Price"=>getIsset('Price')
    );

    if ($conn->update("car_check", $value, array("id" => $id))) {
        alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','carcheckList.php');
    }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
    }

  }else{

    $tbl_car_check = $conn->select('car_check', array('id' => getIsset('id')), true);

    if($tbl_car_check != null){
      $cusID = $tbl_car_check["cusID"];
      $carID = $tbl_car_check["carID"];
      $Date = $tbl_car_check["Date"];
      $ConditionResults = $tbl_car_check["ConditionResults"];
      $Remark = $tbl_car_check["Remark"];
      $Price = $tbl_car_check["Price"];

      $tbl_car = $conn->select('car', array('id' => $carID), true);
      $license = $tbl_car["license"];
      $province_license = $tbl_car["province_license"];
      $registration = convertDateThai($tbl_car["registration"]);
      $typecar = $tbl_car["typecar"];
      $id_document = $tbl_car["id_document"];
      $brand = $tbl_car["brand"];
      $generation = $tbl_car["generation"];
      $body_number = $tbl_car["body_number"];
      $serial_number = $tbl_car["serial_number"];
      $gas_number = $tbl_car["gas_number"];
      $fuel_type = $tbl_car["fuel_type"];
      $capacity = $tbl_car["capacity"];

      $tbl_cus = $conn->select('customer', array('id' => $cusID), true);
      $FName = $tbl_cus["FName"];
      $LName = $tbl_cus["LName"];
      $Address = $tbl_cus["Address"];
      $district = $tbl_cus["district"];
      $amphoe = $tbl_cus["amphoe"];
      $province = $tbl_cus["province"];
      $zipcode = $tbl_cus["zipcode"];
      $Tel = $tbl_cus["Tel"];
    }
  }
}else{
  //Insert
  if ($cmd == 'save') {

    if (intval(getIsset('carID')) > 0 ){
    $value = array(
      "cusID"=>getIsset('cusID')
       ,"carID"=>getIsset('carID')
       ,"Date"=>convertToDateThai(Date('Y-m-d'))
       ,"ConditionResults"=>getIsset('ConditionResults')
       ,"Remark"=>getIsset('Remark')
      ,"Price"=>getIsset('Price')
    );
     if($conn->create("car_check",$value)){
        alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','carcheckList.php');
     }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
     }

   }else{
     alertMassage("โปรดเลือกข้อมูลลูกค้า");
   }
  }
}

if($cmd == 'delete'){
  //delete
    if($conn->delete("car_check",array("id"=>$id))){
        alertMassageAndRedirect('ลบสำเร็จ','carcheckList.php');
    }else{
        alertMassage("ลบไม่สำเร็จ");
    }
}


?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ข้อมูลตรวจสภาพรถ</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../customer">ข้อมูลตรวจสภาพรถ</a></li>
              <li class="breadcrumb-item active">เพิ่มข้อมูลตรวจสภาพรถ</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <form>
      <div class="container-fluid">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">เพิ่มข้อมูลตรวจสภาพรถ</h3>
          </div>
          <div class="col-12 table-responsive card-body">
            <b class="">รายละเอียดข้อมูลเจ้าของรถ</b>
            <hr>
            <div class="card-body">
              <input type="hidden" name="id" value="<?php echo $id ?>">
              <div class="row form-group">
              <label class="ml-4 mt-1 col-sm-2" for="exampleInputEmail1">&nbsp;  ชื่อ-นามสกุล</label>
                <div class="col-8">
                  <input type="hidden" id="CusID"  placeholder="ชื่อ" class="form-control" name="cusID" value="<?php echo $cusID ?>" hidden>
                  <input type="text" id="CusFName" class="form-control ml-2" placeholder="ชื่อ" name="FName" value="<?php echo $FName; ?> <?php echo $LName; ?>" >
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-4 mt-1 col-sm-2" for="exampleInputEmail1">&nbsp;  เบอร์โทร</label>
                <div class="col-8">
                  <input type="number" id="CusTel"  class="form-control ml-2" placeholder="เบอร์โทร" name="Tel" value="<?php echo $Tel; ?>" >
                </div>
              </div>
              
              <div class="row form-group">
                  <label class="ml-4 mt-1 col-sm-2" for="exampleInputEmail1">&nbsp;  ที่อยู่</label>
                <div class="col-8">
                <input type="text" class="form-control ml-2" placeholder="บ้านเลขที่" name="Address" value="<?php echo $Address; ?> ต.<?php echo $district; ?> อ.<?php echo $amphoe; ?> จ.<?php echo $province; ?> <?php echo $zipcode; ?>">
                </div>
              </div>

          </div>

          <hr>
            <b>รายละเอียดรถ</b>
          <hr>

            <div class="row form-group">
                <label class="ml-5 mt-1 col-sm-2" for="exampleInputEmail1">ทะเบียนรถ</label>
              <div class="col-3">
                <input type="text" class="form-control" placeholder="ทะเบียนรถ" name="license" value="<?php echo $license; ?> <?php echo $province_license; ?>" required>
              </div>
              <label class="ml-4 mt-1 mr-4" for="exampleInputEmail1">วันที่จดทะเบียน</label>
              <div class="col-3"> 
                <input type="text" class="form-control" placeholder="วันที่จดทะเบียน"  value="<?php echo $registration; ?>" >
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mt-1 col-sm-2" for="exampleInputEmail1">รย.</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="รย." name="license" value="<?php echo $typecar; ?>" required>
              </div>
            </div>

            <div class="row form-group">
              <label class="ml-5 mt-1 col-sm-2" for="exampleInputEmail1">ยี่ห้อรถ-รุ่น</label>
              <div class="col-8">
              <input type="text" class="form-control" placeholder="ยี่ห้อรถ" name="brand" value="<?php echo $brand; ?> <?php echo $generation; ?>" required>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขตัวถัง</label>
              <div class="col-3">
                <input type="text" class="form-control" placeholder="หมายเลขตัวถัง" name="license" value="<?php echo $body_number; ?>" required>
              </div>
              <label class="ml-4 mt-1 mr-4" for="exampleInputEmail1">หมายเลขเครื่อง</label>
              <div class="col-3">
                <input type="text" class="form-control" placeholder="หมายเลขเครื่อง"  value="<?php echo $serial_number; ?>" required>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขถังแก๊ส</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขถังแก๊ส" name="gas_number" value="<?php echo $gas_number; ?>">
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ชนิดเชื่อเพลิง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ชนิดเชื่อเพลิง" name="fuel_type" value="<?php echo $fuel_type; ?>" required>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ขนาดเครื่องยนต์ (ซีซี)</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ขนาดเครื่องยนต์" name="capacity" value="<?php echo $capacity; ?>" required>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">เลขที่เอกสาร</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="เลขที่เอกสาร" name="id_document" value="<?php echo $id_document; ?>" required>
              </div>
            </div>

            <hr>
              <b>ผลการตรวจสภาพ</b>
            <hr>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ผลการตรวจสภาพ</label>
                <div class="col-8">
                  <input type="text" class="form-control" placeholder="ค่าตรวจสภาพรถ" name="Price" id="price" value="<?php echo $ConditionResults; ?>" >
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเหตุ</label>
                <div class="col-8">
                    <textarea class="form-control" name="Remark" rows="3" placeholder="หมายเหตุ"><?php echo $Remark; ?></textarea>
                </div>
              </div>

              <div class="row form-group">
                    <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ค่าตรวจสภาพรถ</label>
                  <div class="col-8">
                    <input type="text" class="form-control" placeholder="ค่าตรวจสภาพรถ" name="Price" id="price" value="<?php echo $Price; ?>" >
                  </div>
              </div>


        <!-- /.card -->
      </div><!-- /.container-fluid -->
      </form>
    </section>
    <!-- /.content -->
  </div>

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->

<!-- Modal Customer -->
      <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="mediumModalLabel">ข้อมูลลูกค้า</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">

                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                              <th>ลำดับ</th>
                              <th>ชื่อ-นามสกุล</th>
                              <th>เบอร์โทร</th>
                              <th>ทะเบียนรถ</th>
                              <th>จังหวัด</th>
                              <th>เลือก</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php

                                      $index =0;
                                              foreach ($select_all as $row) {
                                                  $index++;

                                                  ?>
                        <tr>
                          <td><?php echo $index; ?></td>
                          <td><?php echo $row['FName']  ?> <?php echo $row['LName'] ?></td>
                          <td><?php echo $row['tel'] ?></td>
                          <td><?php echo $row['license'] ?></td>
                          <td><?php echo $row['province_license'] ?></td>
                          <td align="center"> <button type="button"  class="btn btn-info" data-dismiss="modal"
                            onclick="addCustomer('<?php echo $row['id']; ?>'
                            ,'<?php echo $row['FName']; ?>'
                            ,'<?php echo $row['LName']; ?>'
                            ,'<?php echo $row['tel']; ?>'
                            ,'<?php echo $row['address']; ?>'
                            ,'<?php echo $row['district']; ?>'
                            ,'<?php echo $row['amphoe']; ?>'
                            ,'<?php echo $row['province']; ?>'
                            ,'<?php echo $row['zipcode']; ?>'
                            ,'<?php echo $row['license']; ?>'
                            ,'<?php echo $row['registration']; ?>'
                            ,'<?php echo $row['typecar']; ?>'
                            ,'<?php echo $row['brand']; ?>'
                            ,'<?php echo $row['generation']; ?>'
                            ,'<?php echo $row['serial_number']; ?>'
                            ,'<?php echo $row['gas_number']; ?>'
                            ,'<?php echo $row['fuel_type']; ?>'
                            ,'<?php echo $row['province_license']; ?>'
                            ,'<?php echo $row['body_number']; ?>'
                            ,'<?php echo $row['cusID']; ?>'
                            ,'<?php echo $row['capacity']; ?>'
                            )" >เลือก</button>
                             </td>
                          </tr>

                            <?php
                                       }
                                   ?>

                        </tbody>
                    </table>

                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                  </div>
              </div>
          </div>
      </div>
      <!-- /.modal -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datepicker/locales/bootstrap-datepicker.th.js"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker-thai.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
<script src="../../plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>

<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

<script>
  $(document).ready(function () {
      $('.datepicker').datepicker({
          format: 'dd/mm/yyyy',
          todayBtn: true,
          language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
          thaiyear: true              //Set เป็นปี พ.ศ.
      }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน

      $('#dataTables-example').DataTable({
          "oLanguage": {
              "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
              "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
              "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
              "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
              "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
              "sSearch": "ค้นหา :",
              "oPaginate": {
                  "sFirst": "เิริ่มต้น",
                  "sPrevious": "ก่อนหน้า",
                  "sNext": "ถัดไป",
                  "sLast": "สุดท้าย"
              }
          },
            responsive: true
        });
  });

  function addCustomer(id,FName,LName,Tel,Address,district,amphoe,province,zipcode,license,registration,
    typecar,brand,generation,serialnumber,gasnumber,fueltype,
    provincelicense,Bodynumber,cusID,capacity) {
        document.getElementById("CusID").value = cusID;
        document.getElementById("CusFName").value = FName;
        document.getElementById("CusLName").value = LName;
        document.getElementById("CusAddress").value = Address;
        document.getElementById("Cusdistrict").value = district;
        document.getElementById("Cusamphoe").value = amphoe;
        document.getElementById("Cusprovince").value = province;
        document.getElementById("Cuszipcode").value = zipcode;
        document.getElementById("CusTel").value = Tel;
        document.getElementById("license").value = license;
        document.getElementById("registration").value = registration;
        document.getElementById("typecar").value = typecar;
        document.getElementById("brand").value = brand;
        document.getElementById("generation").value = generation;
        document.getElementById("serialnumber").value = serialnumber;
        document.getElementById("gasnumber").value = gasnumber;
        document.getElementById("fueltype").value = fueltype;
        document.getElementById("provincelicense").value = provincelicense;
        document.getElementById("bodynumber").value = Bodynumber;
        document.getElementById("CarID").value = id;
        document.getElementById("capacity").value = capacity;

        if (typecar == 'รย.12'){
            document.getElementById("price").value = "60";
        }else{
            document.getElementById("price").value = "200";
        }
    }

  function cancelOnclick() {
   window.location = 'carcheckList.php';
  }

</script>

</body>
</html>
