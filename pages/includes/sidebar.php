<?php


        require_once "../../ConnectDatabase/connectionDb.inc.php";

        $date = new DateTime(); // Y-m-d
        $date->add(new DateInterval('P30D'));

        $dateNow = date("Y-m-d");
        $date30Day = $date->format('Y-m-d');

        $dateNow = convertToDateThai($dateNow);
        $date30Day = convertToDateThai($date30Day);

        $sql1 = "SELECT f.id,f.startDate,f.endDate,f.refID,f.status,f.methodtype,f.type,c.license,c.province_license,cm.FName , cm.LName , cm.Tel
                FROM follow f inner join car c on f.carID = c.id
                inner join customer cm on f.cusID = cm.id
                where f.type = 'ต่อ พรบ.' and f.endDate BETWEEN '$dateNow' and '$date30Day' and (f.methodtype IS NULL  or f.methodtype = 'กำลังติดต่อ')   ";

        $select_all1 = $conn->queryRaw($sql1);
        $totalact = sizeof($select_all1);

        $sqltax = "SELECT f.id,f.startDate,f.endDate,f.refID,f.status,f.methodtype,f.type,c.license,c.province_license,cm.FName , cm.LName , cm.Tel
                FROM follow f inner join car c on f.carID = c.id
                inner join customer cm on f.cusID = cm.id
                where f.type = 'ต่อภาษี' and f.endDate BETWEEN '$dateNow' and '$date30Day'  and (f.methodtype IS NULL  or f.methodtype = 'กำลังติดต่อ')";

        $select_alltax = $conn->queryRaw($sqltax);
        $totaltax = sizeof($select_alltax);

        $sqlinsurance = "SELECT f.id,f.startDate,f.endDate,f.refID,f.status,f.methodtype,f.type,c.license,c.province_license,cm.FName , cm.LName , cm.Tel
                FROM follow f inner join car c on f.carID = c.id
                inner join customer cm on f.cusID = cm.id
                where f.type = 'ต่อประกัน' and f.endDate BETWEEN '$dateNow' and '$date30Day'  and (f.methodtype IS NULL  or f.methodtype = 'กำลังติดต่อ') ";

        $select_allinsurance = $conn->queryRaw($sqlinsurance);
        $totalinsurance = sizeof($select_allinsurance);

        $Alltotal = $totalinsurance + $totaltax + $totalact;
        if($Alltotal>0) {
          $All = '<span class="badge navbar-badge"  style="background-color:#FF0000;color:white;font-weight:bolder;">'.$Alltotal.'</span>';
        }else{
          $All ='';
        }

?>

<?php
    $link = $_SERVER['REQUEST_URI'];
    $link_array = explode('/',$link);
    $name = $link_array[count($link_array) - 2];
?>
<nav class="main-header navbar navbar-expand border-bottom navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-3">
      <li class="d-none d-sm-block"><b>ระบบบริหารจัดการ สถานตรวจสภาพรถเอกชน</b></li>
    </ul>
    <ul class="navbar-nav ml-auto">

      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <!-- <i class="fa fa-bell-o"></i> -->
          <img src="../../dist/img/bell.png" alt="" class="img-size-32 img-circle">
            <?php echo $All; ?>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header"><?php echo $Alltotal; ?> การแจ้งเตือน</span>
          <div class="dropdown-divider"></div>
          <a href="../follow/act.php" class="dropdown-item">
            <i class="fas fa-file mr-2 ml-1"></i> <text class="ml-1"><num class="text-primary"> <b><?php echo $totalact; ?></b>  </num> พรบ.</text>
          </a>
          <div class="dropdown-divider"></div>
          <a href="../follow/tax.php" class="dropdown-item">
            <i class="fas fa-money-check mr-2"></i> <num class="text-primary"> <b><?php echo $totaltax; ?> </b>  </num>ภาษี
          </a>
          <div class="dropdown-divider"></div>
          <a href="../follow/insurance.php" class="dropdown-item">
            <i class="fas fa-ambulance mr-2"></i> <num class="text-primary"> <b><?php echo $totalinsurance; ?> </b>  </num>ประกันภาคสมัครใจ
          </a>
      </li>


      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
        <img src="../../dist/img/logochangyai-128x128.png" alt="" class="img-size-32 mr-3 img-circle">
          <span class="badge navbar-badge"  style="background-color:#1ae849;color:white;font-weight:bolder;"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="../../dist/img/logochangyai-128x128.png" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  ตรอ. ช่างใหญ่เซอร์วิส
                  <span class="float-right text-sm text-primary"><i class="fa fa-star"></i></span>
                </h3>
                <p class="text-sm"><?php echo $_SESSION["Name"] ?></p>
                <p class="text-sm text-muted"></i> พนักงาน </p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider" ></div>
          <a href="../../logout.php" class="dropdown-item dropdown-footer">
            <i class="fas fa-sign-out-alt"></i>
            ออกจากระบบ
          </a>
        </div>
      </li>
    </ul>
</nav>
  <!-- /.navbar -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../dashboard/" class="brand-link">
      <img  src="../../dist/img/logochangyai-128x128.png" 
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">ช่างใหญ่เซอร์วิส</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel d-flex">
        <div class="image">
        
        </div>
        <div class="info">
          <a href="../dashboard/" class="d-block">สถานตรวจสภาพรถเอกชน</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2" >
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- <li class="nav-item">
            <a href="../dashboard" class="nav-link <?php echo $name == 'dashboard' ? 'active': '' ?>">
              <i class="fas fa-tachometer-alt nav-icon"></i>
              <p>Dashboard</p>
            </a>
          </li> -->
          <li class="nav-item">
            <a href="../customer/" class="nav-link <?php echo $name == 'customer' ? 'active': '' ?>">
              <i class="fas fa-chalkboard-teacher nav-icon"></i>
              <p>ข้อมูลลูกค้า</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../car/carList.php" class="nav-link <?php echo $name == 'car' ? 'active': '' ?>">
              <i class="fas fa-car nav-icon"></i>
              <p>ข้อมูลรถ</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../carcheck/carcheckList.php" class="nav-link <?php echo $name == 'carcheck' ? 'active': '' ?>">
              <i class="fas fa-truck-moving nav-icon"></i>
              <p>ข้อมูลตรวจสภาพรถ</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../act/actList.php" class="nav-link <?php echo $name == 'act' ? 'active': '' ?>">
              <i class="fas fa-file-alt nav-icon"></i>
              <p>ข้อมูลการต่อ พรบ.</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../tax/taxList.php" class="nav-link <?php echo $name == 'tax' ? 'active': '' ?>">
              <i class="fas fa-money-check nav-icon"></i>
              <p>ข้อมูลการต่อภาษี</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../insurance/insuranceList.php" class="nav-link <?php echo $name == 'insurance' ? 'active': '' ?>">
              <i class="fas fa-ambulance nav-icon"></i>
              <p>ข้อมูลการต่อประกัน</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="../transfer/transferList.php" class="nav-link <?php echo $name == 'transfer' ? 'active': '' ?>">
              <i class="fas fa-exchange-alt nav-icon"></i>
              <p>ข้อมูลการโอน</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="../move/moveList.php" class="nav-link <?php echo $name == 'move' ? 'active': '' ?>">
              <i class="fas fa-expand-arrows-alt nav-icon"></i>
              <p>ข้อมูลการย้ายทะเบียน</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="../change/changeList.php" class="nav-link <?php echo $name == 'change' ? 'active': '' ?>">
              <i class="fas fa-edit nav-icon"></i>
              <p>ข้อมูลเปลี่ยนทะเบียนรถ</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="../insurancename/" class="nav-link <?php echo $name == 'insurancename' ? 'active': '' ?>">
              <i class="far fa-building nav-icon"></i>
              <p>รายชื่อบริษัทประกันภัย</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="../autocomplete/province.php" class="nav-link <?php echo $name == 'province' ? 'active': '' ?>">
              <i class="far fa-building nav-icon"></i>
              <p>รายชื่อจังหวัด</p>
            </a>
          </li>
          
           <li class="nav-item has-treeview">
           <a href="" class="nav-link <?php echo $name == 'follow' ? 'active': '' ?>">
              <i class="nav-icon fa fa-calendar"></i>
              <p>
                ติดตามลูกค้า
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../follow/act.php" class="nav-link">
                  <i class="fa fa-angle-right nav-icon"></i>
                  <p>
                    พรบ.
                  </p>
                </a>
              </li>
              <li class="nav-item">
               <a href="../follow/tax.php" class="nav-link">
                  <i class="fa fa-angle-right nav-icon"></i>
                  <p>
                    ภาษี
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../follow/insurance.php" class="nav-link">
                  <i class="fa fa-angle-right nav-icon"></i>
                  <p>
                    ประกันภาคสมัครใจ
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../follow/followed.php" class="nav-link">
                  <i class="fa fa-angle-right nav-icon"></i>
                  <p>
                    รายการที่ติดตามแล้ว
                  </p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="../importExcel/importExcel.php" class="nav-link <?php echo $name == 'importExcel' ? 'active': '' ?>">
              <i class="fas fa-edit nav-icon"></i>
              <p>Import Excel</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../ExportExcel/ExportExcel.php" class="nav-link <?php echo $name == 'ExportExcel' ? 'active': '' ?>">
              <i class="fas fa-edit nav-icon"></i>
              <p>Export Excel</p>
            </a>
          </li>
          <li class="nav-header">Account Settings</li>
          <li class="nav-item">
            <a href="../../logout.php" class="nav-link">
              <i class="fas fa-sign-out-alt"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
