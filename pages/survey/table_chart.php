<?php
	session_start();
	require_once "../../ConnectDatabase/connectionDb.inc.php";
	require_once('../../php/connect.php');  

	$sql = "SELECT count(id_person) as qtyperson FROM tb_person where created BETWEEN '".convertToYmd($_POST['start_date'])."' and '".convertToYmd($_POST['end_date'])."'  ";
	$result = $conn->query($sql) or die($conn->error);
	$row = $result->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Survey Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
  </head>
  <body>
    <h2 class="text-center m-3">ผลประเมินจากจำนวนคนทั้งหมด <?php echo $row['qtyperson'] ?> คน </h2>

		<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
		
		<table class="table" id="datatable">
			<thead>
				<tr>
					<th>คำถาม</th>
					<th>คะแนนรวม</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$sql = "SELECT tb_question.*,tb_answer.id_question, sum(tb_answer.scores) as qtyscore FROM tb_answer LEFT JOIN tb_question 
					ON tb_answer.id_question = tb_question.id_question inner join tb_person ps where ps.created BETWEEN '".convertToYmd($_POST['start_date'])."' and '".convertToYmd($_POST['end_date'])."' GROUP BY tb_answer.id_question  ";
					
					$result = $conn->query($sql) or die($conn->error);
					while($row = $result->fetch_assoc()){
						echo"<tr>";
							echo "<td>".$row['question']."</td>";
							echo "<td>".$row['qtyscore']."</td>";
							
						echo"</tr>";
					}
				?>
			
			</tbody>
		</table>
		<table class="table" >
			<tfoot>
			<tr>
				<td colspan="2">
				<a href="index.php" class="btn btn-danger float-right mx-2">
						กลับ
					</a>  
				</td>
			</tr>
			</tfoot>
		</table>

		
	<script src="../../plugins/chart/jquery-1.12.0.min.js"></script>
	<script src="../../plugins/chart/highcharts.js"></script>
	<script src="../../plugins/chart/data.js"></script>
	<script src="../../plugins/chart/exporting.js"></script>
	
	<script>
	
	$(function () {
				
		$('#container').highcharts({
			data: {
				//กำหนดให้ ตรงกับ id ของ table ที่จะแสดงข้อมูล
				table: 'datatable'
			},
			chart: {
				type: 'column'
			},
			title: {
				text: 'กราฟแสดงผลประเมินการให้บริการ'
			},
			yAxis: {
				allowDecimals: false,
				title: {
					text: 'Units'
				}
			},
			
			tooltip: {
				formatter: function () {
					return '<b>' + this.series.name + '</b><br/>' +
						this.point.y; + ' ' + this.point.name.toLowerCase();
				}
			}
		});
	});
	
	</script>
	
  </body>
</html>






