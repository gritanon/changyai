<?php
  session_start();

  require_once('../../php/connect.php');  

  $sql="SELECT count(id) as qtycus FROM customer";
  $result = $conn->query($sql) or die($conn->error);
  $row_cus = $result->fetch_assoc();

  $sql2="SELECT count(id) as qtycar FROM car";
  $result = $conn->query($sql2) or die($conn->error);
  $row_car = $result->fetch_assoc();

  $sql3="SELECT count(id) as qtycheckcar FROM car_check";
  $result = $conn->query($sql3) or die($conn->error);
  $row_checkcar = $result->fetch_assoc();

  $sql4="SELECT count(id) as qtyact FROM act";
  $result = $conn->query($sql4) or die($conn->error);
  $row_act = $result->fetch_assoc();

  $sql5="SELECT count(id) as qtytax FROM tax";
  $result = $conn->query($sql5) or die($conn->error);
  $row_tax = $result->fetch_assoc();

  $sql6="SELECT count(id) as qtyinsurance FROM insurance";
  $result = $conn->query($sql6) or die($conn->error);
  $row_insurance = $result->fetch_assoc();

  $sql7="SELECT count(id) as qtytransfer FROM transfer";
  $result = $conn->query($sql7) or die($conn->error);
  $row_transfer = $result->fetch_assoc();

  $sql8="SELECT count(id) as qtymove FROM move";
  $result = $conn->query($sql8) or die($conn->error);
  $row_move = $result->fetch_assoc();

  $sql9="SELECT count(id) as qtychange FROM change_car";
  $result = $conn->query($sql9) or die($conn->error);
  $row_change = $result->fetch_assoc();

  $sql10 = "SELECT count(results) as come FROM follow
  where results = 'กลับมาใช้บริการ' ";
  $result = $conn->query($sql10) or die($conn->error);
  $row_come = $result->fetch_assoc();

  $sql11 = "SELECT count(results) as notcome FROM follow
    where results = 'เลิกติดตาม' ";
  $result = $conn->query($sql11) or die($conn->error);
  $row_notcome = $result->fetch_assoc();

  $sql12 = "SELECT count(id_person) as qtyperson FROM tb_person";
  $result = $conn->query($sql12) or die($conn->error);
  $row_person = $result->fetch_assoc();



  ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <?php include_once('../includes/check_sidebar.php') ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
        
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <h5 class="mb-2">จำนวนลูกค้า รถ และผลการติดตาม</h5>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
               
                <span class="info-box-icon bg-info"><a href="../customer/"><i class="fas fa-chalkboard-teacher"></i> </a></span>
           
              <div class="info-box-content">
                <span class="info-box-text">ลูกค้า</span>
                <span class="info-box-number"><?php echo $row_cus['qtycus'] ?> คน</span>
              </div>

              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><a href="../car/carlist.php"><i class="fas fa-car"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">รถ</span>
                <span class="info-box-number"><?php echo $row_car['qtycar'] ?> คัน</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><i class="fas fa-user-plus"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">กลับมาใช้บริการ</span>
                <span class="info-box-number"><?php echo $row_come['come'] ?> คน</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-danger"><i class="fas fa-user-minus"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">เลิกติดตาม</span>
                <span class="info-box-number"><?php echo $row_notcome['notcome'] ?> คน</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>


        </div>
        <!-- /.row -->
        <h5 class="mt-2 mb-2">จำนวนการใช้บริการ<code></code></h5>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-info">
              <span class="info-box-icon"><i class="fas fa-truck-moving"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ตรวจสภาพรถ</span>
                <span class="info-box-number"><?php echo $row_checkcar['qtycheckcar'] ?> รายการ</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 100%"></div>
                </div>
                <span class="progress-description">
                  <a href="../carcheck/carcheckList.php">รายละเอียดเพิ่มเติม</a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-success">
              <span class="info-box-icon"><i class="fas fa-file"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">พรบ.</span>
                <span class="info-box-number"><?php echo $row_act['qtyact'] ?> รายการ</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 100%"></div>
                </div>
                <span class="progress-description">
                <a href="../act/actList.php">รายละเอียดเพิ่มเติม</a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-warning">
              <span class="info-box-icon"><i class="fas fa-ambulance"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ประกันภาคสมัครใจ</span>
                <span class="info-box-number"><?php echo $row_insurance['qtyinsurance'] ?> รายการ</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 100%"></div>
                </div>
                <span class="progress-description">
                  <a href="../insurance/insuranceList.php">รายละเอียดเพิ่มเติม</a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-danger">
              <span class="info-box-icon"><i class="fas fa-money-check"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ภาษี</span>
                <span class="info-box-number"><?php echo $row_tax['qtytax'] ?> รายการ</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 100%"></div>
                </div>
                <span class="progress-description">
                <a href="../tax/taxList.php">รายละเอียดเพิ่มเติม</a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-info">
              <span class="info-box-icon"><i class="fas fa-exchange-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">โอน</span>
                <span class="info-box-number"><?php echo $row_transfer['qtytransfer'] ?> รายการ</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 100%"></div>
                </div>
                <span class="progress-description">
                <a href="../transfer/transferList.php">รายละเอียดเพิ่มเติม</a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-success">
              <span class="info-box-icon"><i class="fas fa-expand-arrows-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ย้ายทะเบียน</span>
                <span class="info-box-number"><?php echo $row_move['qtymove'] ?> รายการ</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 100%"></div>
                </div>
                <span class="progress-description">
                <a href="../move/moveList.php">รายละเอียดเพิ่มเติม</a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-warning">
              <span class="info-box-icon"><i class="fas fa-edit"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">เปลี่ยนทะเบียน</span>
                <span class="info-box-number"><?php echo $row_change['qtychange'] ?> รายการ</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 100%"></div>
                </div>
                <span class="progress-description">
                <a href="../change/changeList.php">รายละเอียดเพิ่มเติม</a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          
        </div>
        <!-- /.row -->

         <h5 class="mt-2 mb-2">จำนวนผู้ตอบแบบประเมินหน้าเว็บไซต์<code></code></h5>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-info">
              <span class="info-box-icon"><i class="fas fa-chalkboard-teacher"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ผู้ตอบแบบประเมิน</span>
                <span class="info-box-number"><?php echo $row_person['qtyperson'] ?> คน</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 100%"></div>
                </div>
                <span class="progress-description">
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

      
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>

</body>
</html>
