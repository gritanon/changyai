<?php

session_start();
require_once "../../ConnectDatabase/connectionDb.inc.php";

$cmd = getIsset("__cmd");

$carID = $_GET["id"];
$cusID = $_GET["cusID"];

$sql = "SELECT c.id,cus.FName,cus.LName,cus.Address,cus.province,cus.district,cus.amphoe,cus.zipcode,cus.tel,c.license,c.registration,c.typecar,c.brand,c.generation,c.serial_number,c.gas_number,c.fuel_type,
c.province_license,c.body_number,cus.id as cusID,c.capacity 
from car c inner join customer cus on c.cusID = cus.id WHERE c.id='".$carID."' and c.cusID = '".$cusID."' ";
$select_all = $conn->queryRaw($sql);

$index =0;
foreach ($select_all as $row) {
    $index++;


if (intval($id) > 0){
  //Update
  if ($cmd == 'save') {
    $value = array(
      "cusID"=>getIsset('cusID')
       ,"carID"=>getIsset('carID')
       ,"type"=>getIsset('typeCompany')
      ,"Price"=>getIsset('Price')
      ,"startDate"=>convertToYmd(getIsset('startDate'))
     ,"endDate"=>convertToYmd(getIsset('endDate'))
    );

    if (getIsset('typecar') == 'รย.12'){
      if (getIsset('typeCompany') != 'บริษัท กลางคุ้มครองผู้ประสบภัยจากรถ จำกัด'){
           alertMassage("รย.12 ใช้ พรบ. ของบริษัท กลางคุ้มครองผู้ประสบภัยจากรถ จำกัดเท่านั้น");
      }else{
        if ($conn->update("act", $value, array("id" => $id))) {

          UpdateFollow(getIsset('cusID'),getIsset('carID'),getIsset('startDate'),getIsset('endDate'));
          alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','actList.php');

        }else{
           alertMassage("ไม่สามารถบันทึกข้อมูลได้");
        }
      }
    }else {
      if ($conn->update("act", $value, array("id" => $id))) {
          UpdateFollow(getIsset('cusID'),getIsset('carID'),getIsset('startDate'),getIsset('endDate'));

          alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','actList.php');
       }else{
          alertMassage("ไม่สามารถบันทึกข้อมูลได้");
       }
    }
  }else{

    $tbl_act = $conn->select('act', array('id' => getIsset('id')), true);

    if($tbl_act != null){
      $cusID = $tbl_act["cusID"];
      $carID = $tbl_act["carID"];
      $Date = $tbl_act["Date"];
      $type = $tbl_act["type"];
      $Price = $tbl_act["Price"];
      $startDate = convertTodmy($tbl_act["startDate"]);
      $endDate = convertTodmy($tbl_act["endDate"]);

      $tbl_car = $conn->select('car', array('id' => $carID), true);
      $license = $tbl_car["license"];
      $province_license = $tbl_car["province_license"];
      $registration = convertDateThai($tbl_car["registration"]);

      $typecar = $tbl_car["typecar"];
      $brand = $tbl_car["brand"];
      $generation = $tbl_car["generation"];
      $body_number = $tbl_car["body_number"];
      $serial_number = $tbl_car["serial_number"];
      $gas_number = $tbl_car["gas_number"];
      $fuel_type = $tbl_car["fuel_type"];
      $capacity = $tbl_car["capacity"];

      $tbl_cus = $conn->select('customer', array('id' => $cusID), true);
      $FName = $tbl_cus["FName"];
      $LName = $tbl_cus["LName"];
      $Address = $tbl_cus["Address"];
      $district = $tbl_cus["district"];
      $amphoe = $tbl_cus["amphoe"];
      $province = $tbl_cus["province"];
      $zipcode = $tbl_cus["zipcode"];
      $Tel = $tbl_cus["Tel"];
    }
  }
}else{
  //Insert
  if ($cmd == 'save') {

    if (intval(getIsset('carID')) > 0 ){

      $value = array(
        "cusID"=>getIsset('cusID')
         ,"carID"=>getIsset('carID')
         ,"Date"=>convertToDateThai(Date('Y-m-d'))
         ,"type"=>getIsset('typeCompany')
        ,"Price"=>getIsset('Price')
        ,"startDate"=>convertToYmd(getIsset('startDate'))
       ,"endDate"=>convertToYmd(getIsset('endDate'))
      );


      if (getIsset('typecar') == 'รย.12'){
        if (getIsset('typeCompany') != 'บริษัท กลางคุ้มครองผู้ประสบภัยจากรถ จำกัด'){
             alertMassage("รย.12 ใช้ พรบ. ของบริษัท กลางคุ้มครองผู้ประสบภัยจากรถ จำกัดเท่านั้น");
        }else{
          if($conn->create("act",$value)){
             alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','actList.php');
          }else{
             alertMassage("ไม่สามารถบันทึกข้อมูลได้");
          }
        }
      }else {
         if($conn->create("act",$value)){
           $lastID = $conn->getLastInsertId();
           $value1 = array(
             "refID"=>$lastID
             ,"cusID"=>getIsset('cusID')
              ,"carID"=>getIsset('carID')
              ,"type"=>'ต่อ พรบ.'
              ,"startDate"=>convertToYmd(getIsset('startDate'))
             ,"endDate"=>convertToYmd(getIsset('endDate'))
           );

           if($conn->create("follow",$value1)){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','actList.php');
          }
         }else{
            alertMassage("ไม่สามารถบันทึกข้อมูลได้");
         }
      }
   }else{
     alertMassage("โปรดเลือกข้อมูลลูกค้า");
   }
  }elseif ($cmd == 'savetax') {


    if (intval(getIsset('carID')) > 0 ){

      $value = array(
        "cusID"=>getIsset('cusID')
         ,"carID"=>getIsset('carID')
         ,"Date"=>convertToDateThai(Date('Y-m-d'))
         ,"type"=>getIsset('typeCompany')
        ,"Price"=>getIsset('Price')
        ,"startDate"=>getIsset('startDate')
       ,"endDate"=>getIsset('endDate')
      );


      if (getIsset('typecar') == 'รย.12'){
        if (getIsset('typeCompany') != 'บริษัท กลางคุ้มครองผู้ประสบภัยจากรถ จำกัด'){
             alertMassage("รย.12 ใช้ พรบ. ของบริษัท กลางคุ้มครองผู้ประสบภัยจากรถ จำกัดเท่านั้น");
        }else{
          if($conn->create("act",$value)){
             alertMassageAndRedirect('ไปภาษี','../tax/taxInfos.php?id='.getIsset('carID').'&cusID='.$cusID);
          }else{
             alertMassage("ไม่สามารถบันทึกข้อมูลได้");
          }
        }
      }else {
         if($conn->create("act",$value)){
           $lastID = $conn->getLastInsertId();
           $value1 = array(
             "refID"=>$lastID
             ,"cusID"=>getIsset('cusID')
              ,"carID"=>getIsset('carID')
              ,"type"=>'ต่อ พรบ.'
             ,"startDate"=>getIsset('startDate')
            ,"endDate"=>getIsset('endDate')
           );

           if($conn->create("follow",$value1)){
            alertMassageAndRedirect('ไปภาษี','../tax/taxInfos.php?id='.getIsset('carID').'&cusID='.$cusID);
          }
         }else{
            alertMassage("ไม่สามารถบันทึกข้อมูลได้");
         }
      }
   }else{
     alertMassage("โปรดเลือกข้อมูลลูกค้า");
   }
  }
}

if($cmd == 'delete'){
  //delete
    if($conn->delete("act",array("id"=>$id))){
        alertMassageAndRedirect('ลบสำเร็จ','actList.php');
    }else{
        alertMassage("ลบไม่สำเร็จ");
    }
}


?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- insurance list -->
    <link rel="stylesheet" href="../../plugins/insurance/css/jquery-ui-1.10.2.custom.css" />
  <script src="../../plugins/insurance/js/jquery-1.9.1.js"></script>
  <script src="../../plugins/insurance/js/jquery-ui-1.10.2.custom.min.js"></script>
</head>
<body class="hold-transition sidebar-mini" onload="addCustomer('<?php echo $row['id']; ?>'
                            ,'<?php echo $row['FName']; ?>'
                            ,'<?php echo $row['LName']; ?>'
                            ,'<?php echo $row['tel']; ?>'
                            ,'<?php echo $row['address']; ?>'
                            ,'<?php echo $row['district']; ?>'
                            ,'<?php echo $row['amphoe']; ?>'
                            ,'<?php echo $row['province']; ?>'
                            ,'<?php echo $row['zipcode']; ?>'
                            ,'<?php echo $row['license']; ?>'
                            ,'<?php echo $row['registration']; ?>'
                            ,'<?php echo $row['typecar']; ?>'
                            ,'<?php echo $row['brand']; ?>'
                            ,'<?php echo $row['generation']; ?>'
                            ,'<?php echo $row['serial_number']; ?>'
                            ,'<?php echo $row['gas_number']; ?>'
                            ,'<?php echo $row['fuel_type']; ?>'
                            ,'<?php echo $row['province_license']; ?>'
                            ,'<?php echo $row['body_number']; ?>'
                            ,'<?php echo $row['cusID']; ?>'
                            ,'<?php echo $row['capacity']; ?>'
                            ,'<?php echo $row['weight']; ?>'
                            )">
<!-- Site wrapper -->

                            <?php
                                       }
                                   ?>
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/check_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ข้อมูลการต่อ พรบ.</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../customer">ข้อมูลการต่อ พรบ.</a></li>
              <li class="breadcrumb-item active">เพิ่มข้อมูลการต่อ พรบ.</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <form>
      <div class="container-fluid">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">เพิ่มข้อมูลการต่อ พรบ.</h3>
          </div>
          <div class="col-12 table-responsive card-body">
            <b class="">รายละเอียดข้อมูลเจ้าของรถ</b>
            <hr>
            <div class="card-body">
              <input type="hidden" name="id" id="cusID" value="<?php echo $id ?>">
              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ชื่อ</label>
              <?php if (intval($id) == 0) { ?>
                <div class="col-8">
                <?php } else { ?>
                <div class="col-10">
                <?php } ?>
                  <input type="hidden" id="CusID"  placeholder="ชื่อ" class="form-control" name="cusID" value="<?php echo $cusID ?>" hidden>
                  <input type="hidden" id="CarID"  placeholder="รถ" class="form-control" name="carID" value="<?php echo $carID ?>" hidden>

                  <input type="text" id="CusFName" class="form-control" placeholder="ชื่อ" name="FName" value="<?php echo $FName; ?>" readonly readonly>
                </div>
                <?php if (intval($id) == 0) { ?>
                <div class="col-2">
                  <button type="button" class="btn btn-default mb-1" data-toggle="modal" data-target="#mediumModal">
                   เพิ่มข้อมูลลูกค้า
                  </button>
               </div>
              <?php } ?>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">นามสกุล</label>
                <div class="col-10">
                  <input type="text" id="CusLName"  class="form-control" placeholder="นามสกุล" name="LName" value="<?php echo $LName; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">เบอร์โทร</label>
                <div class="col-10">
                  <input type="number" id="CusTel"  class="form-control" placeholder="เบอร์โทร" name="Tel" value="<?php echo $Tel; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ที่อยู่</label>
                <div class="col-10">
                <input type="text"  id="CusAddress" class="form-control" placeholder="ที่อยู่" name="Address" value="<?php echo $Address; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
              <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1"></label>
                <div class="col-5">
                <input type="text"  id="Cusdistrict" class="form-control" placeholder="ตำบล / แขวง" name="district" value="<?php echo $district; ?>" readonly>
                </div>
                <div class="col-5">
                <input type="text"  id="Cusamphoe" class="form-control" placeholder="อำเภอ" name="amphoe" value="<?php echo $amphoe; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
              <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1"></label>
                <div class="col-5">
                <input type="text"  id="Cusprovince" class="form-control" placeholder="จังหวัด" name="province" value="<?php echo $province; ?>" readonly>
                </div>
                <div class="col-5">
                <input type="text"  id="Cuszipcode" class="form-control" placeholder="รหัสไปรษณีย์" name="zipcode" value="<?php echo $zipcode; ?>" readonly>
                </div>
              </div>

          </div>

          <hr>
            <b>รายละเอียดรถ</b>
          <hr>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ทะเบียนรถ</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ทะเบียนรถ" id="license" name="license" value="<?php echo $license; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">จังหวัด</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="จังหวัด" id="provincelicense" name="province_license" value="<?php echo $province_license; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">วันที่จดทะเบียน</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="วันที่จดทะเบียน" id="registration" name="registration" value="<?php echo $registration; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">รย.</label>
              <div class="col-8">
               <input type="text" class="form-control" placeholder="รย." id="typecar" name="typecar" value="<?php echo $typecar; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ยี่ห้อรถ</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ยี่ห้อรถ" id="brand" name="brand" value="<?php echo $brand; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">รุ่น</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="รุ่น" id="generation" name="generation" value="<?php echo $generation; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขตัวถัง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขตัวถัง" id="bodynumber" name="body_number" value="<?php echo $body_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขเครื่อง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขเครื่อง" id="serialnumber" name="serial_number" value="<?php echo $serial_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขถังแก๊ส</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขถังแก๊ส" id="gasnumber" name="gas_number" value="<?php echo $gas_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ชนิดเชื่อเพลิง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ชนิดเชื่อเพลิง" id="fueltype" name="fuel_type" value="<?php echo $fuel_type; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ขนาดเครื่องยนต์ (ซีซี)</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ขนาดเครื่องยนต์" id="capacity" name="capacity" value="<?php echo $capacity; ?>" readonly>
              </div>
            </div>

            <hr>
              <b>ข้อมูลการต่อ พรบ.</b>
            <hr>
            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">วันเริ่มต้นการคุ้มครอง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="วันเริ่มต้นคุ้มครอง" id="startDate" name="startDate" value="<?php echo $startDate; ?>" onchange="selectStartDate()" required>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">วันสิ้นสุดการคุ้มครอง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="วันสิ้นสุดการคุ้มครอง" id="endDate" name="endDate" value="<?php echo $endDate; ?>" required>
              </div>
            </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">บริษัทคุ้มครอง</label>
                <div class="col-6">
                <?php
                      require_once('../../php/connect.php');
                  		$comma = '';
                      $allEmp = '';
                      $sql2="SELECT id,Name FROM insurancecompany ORDER BY Name ASC";
                      $resultin = $conn->query($sql2) or die($conn->error);
                      while($row = $resultin->fetch_assoc()) {
                        $allEmp .= $comma.'{value: "'.$row['id'].'",label: "'.$row['Name'].'"}';
                        if($comma==='') $comma = ',';
                      }
                      //การใช้งานจริง ส่วนนี้จะถูกเขียนเป็นไฟล์ .js เพื่อเรียกใช้ใน javascript
                      $allEmp = '['. $allEmp .']';
                    ?>
                <input id="Name"  class="form-control" name="typeCompany"  placeholder="บรัษัทประกันภัย" value="<?php echo $type;?>">
                <input type="hidden" id="project-id" />
                </div>
                <div class="col-2">
                  <button type="button" class="btn btn-default mb-1" data-toggle="modal" data-target="#mediumModal2">
                   เพิ่มตัวเลือก
                  </button>
               </div>
              </div>

              <div class="row form-group">
                    <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">อัตราค่าเบี้ย พรบ.</label>
                  <div class="col-8">
                    <input type="text" class="form-control" placeholder="อัตราค่าเบี้ย พรบ." name="Price" id="price" value="<?php if ($typecar == 'รย.12'){
                        echo "324";
                      }elseif($typecar == 'รย.1'){
                        echo "645";
                      }elseif($typecar == 'รย.2'){
                        echo "1282";
                      }elseif($typecar == 'รย.3'){
                        echo "967";
                      } ?>" >
                  </div>
              </div>

            <div class="form-group">
             <center>
                 <button type="submit" class="btn btn-primary" name="__cmd" value="save">ตกลง</button>
                 <!-- <button type="submit" class="btn btn-primary" name="__cmd" value="savetax">บันทึกและต่อภาษี</button> -->
                 <button type="reset" class="btn btn-default" onclick="cancelOnclick()">ยกเลิก</button>
               </center>
           </div>

        <!-- /.card -->
      </div><!-- /.container-fluid -->
      </form>
    </section>
    <!-- /.content -->
  </div>

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->

<!-- Modal Customer -->
      <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="mediumModalLabel">ข้อมูลลูกค้า</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">

                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                              <th>ลำดับ</th>
                              <th>ชื่อ-นามสกุล</th>
                              <th>เบอร์โทร</th>
                              <th>ทะเบียนรถ</th>
                              <th>จังหวัด</th>
                              <th>เลือก</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php

                                      $index =0;
                                              foreach ($select_all as $row) {
                                                  $index++;

                                                  ?>
                        <tr>
                          <td><?php echo $index; ?></td>
                          <td><?php echo $row['FName']  ?> <?php echo $row['LName'] ?></td>
                          <td><?php echo $row['tel'] ?></td>
                          <td><?php echo $row['license'] ?></td>
                          <td><?php echo $row['province_license'] ?></td>
                          <td align="center"> <button type="button"  class="btn btn-info" data-dismiss="modal"
                            onclick="addCustomer('<?php echo $row['id']; ?>'
                            ,'<?php echo $row['FName']; ?>'
                            ,'<?php echo $row['LName']; ?>'
                            ,'<?php echo $row['tel']; ?>'
                            ,'<?php echo $row['address']; ?>'
                            ,'<?php echo $row['district']; ?>'
                            ,'<?php echo $row['amphoe']; ?>'
                            ,'<?php echo $row['province']; ?>'
                            ,'<?php echo $row['zipcode']; ?>'
                            ,'<?php echo $row['license']; ?>'
                            ,'<?php echo $row['registration']; ?>'
                            ,'<?php echo $row['typecar']; ?>'
                            ,'<?php echo $row['brand']; ?>'
                            ,'<?php echo $row['generation']; ?>'
                            ,'<?php echo $row['serial_number']; ?>'
                            ,'<?php echo $row['gas_number']; ?>'
                            ,'<?php echo $row['fuel_type']; ?>'
                            ,'<?php echo $row['province_license']; ?>'
                            ,'<?php echo $row['body_number']; ?>'
                            ,'<?php echo $row['cusID']; ?>'
                            ,'<?php echo $row['capacity']; ?>'
                            )" >เลือก</button>
                             </td>
                          </tr>

                            <?php
                                       }
                                   ?>

                        </tbody>
                    </table>

                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                  </div>
              </div>
          </div>
      </div>
      <!-- /.modal -->

      <!-- Modal Customer -->
      <div class="modal fade" id="mediumModal2" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
          <form action="add_insurance.php" method="post">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="mediumModalLabel">เพิ่มบริษัทประกันภัย</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                  <div class="row">

                  <!-- general form elements -->
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>ชื่อบริษัท</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="text" class="form-control" name="name">
                            </div>
                          </div>
                        </div>
                      </div>

                  </div>
                  <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" >เพิ่ม</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                  </div>
              </div>
              </form>
          </div>
      </div>
      <!-- /.modal -->

<!-- jQuery -->

<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>

<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
<script src="../../plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>
<!-- insurance -->
<script type="text/javascript" src="../../plugins/insurance/themes/js/allEmpData.js"></script>

<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function () {

    var cusID  = document.getElementById('cusID').value;

    if (cusID == ""){
      var date_input=$('input[name="startDate"]'); //our date input has the name "date"
           var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
           var options={
             format: 'dd-mm-yyyy',
             container: container,
             todayHighlight: true,
             language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
             thaiyear: true   ,
             autoclose: true,

           };
           date_input.datepicker(options);

        var date_input1=$('input[name="endDate"]'); //our date input has the name "date"
        var container1=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        var options1={
            format: 'dd-mm-yyyy',
            container: container1,
            todayHighlight: true,
            language: 'th',            //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
             thaiyear: true   ,
             autoclose: true,  
                     //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
            // startDate: document.getElementById('startDate').value,
         };
            date_input1.datepicker(options1);
      }else {
        var date_input=$('input[name="startDate"]'); //our date input has the name "date"
             var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
             var options={
               format: 'dd-mm-yyyy',
               container: container,
               todayHighlight: true,
               language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
              // thaiyear: true   ,
               autoclose: true,

             };
             date_input.datepicker(options);

          var date_input1=$('input[name="endDate"]'); //our date input has the name "date"
          var container1=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
          var options1={
              format: 'dd-mm-yyyy',
              container: container1,
              todayHighlight: true,
              language: 'th',           //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
              // thaiyear: true ,
              autoclose: true,
              // startDate: document.getElementById('startDate').value,
           };
              date_input1.datepicker(options1);
      }

      $('#dataTables-example').DataTable({
          "oLanguage": {
              "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
              "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
              "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
              "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
              "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
              "sSearch": "ค้นหา :",
              "oPaginate": {
                  "sFirst": "เิริ่มต้น",
                  "sPrevious": "ก่อนหน้า",
                  "sNext": "ถัดไป",
                  "sLast": "สุดท้าย"
              }
          },
            responsive: true
        });
  });

  function addCustomer(id,FName,LName,Tel,Address,district,amphoe,province,zipcode,license,registration,
    typecar,brand,generation,serialnumber,gasnumber,fueltype,
    provincelicense,Bodynumber,cusID,capacity) {
        document.getElementById("CusID").value = cusID;
        document.getElementById("CusFName").value = FName;
        document.getElementById("CusLName").value = LName;
        document.getElementById("CusAddress").value = Address;
        document.getElementById("Cusdistrict").value = district;
        document.getElementById("Cusamphoe").value = amphoe;
        document.getElementById("Cusprovince").value = province;
        document.getElementById("Cuszipcode").value = zipcode;
        document.getElementById("CusTel").value = Tel;
        document.getElementById("license").value = license;
        document.getElementById("registration").value = registration;
        document.getElementById("typecar").value = typecar;
        document.getElementById("brand").value = brand;
        document.getElementById("generation").value = generation;
        document.getElementById("serialnumber").value = serialnumber;
        document.getElementById("gasnumber").value = gasnumber;
        document.getElementById("fueltype").value = fueltype;
        document.getElementById("provincelicense").value = provincelicense;
        document.getElementById("bodynumber").value = Bodynumber;
        document.getElementById("CarID").value = id;
        document.getElementById("capacity").value = capacity;

        if (typecar == 'รย.12'){
            document.getElementById("price").value = "324";
        }else if (typecar == 'รย.1'){
            document.getElementById("price").value = "645";
        }else if (typecar == 'รย.2'){
            document.getElementById("price").value = "1282";
        }else if (typecar == 'รย.3'){
            document.getElementById("price").value = "967";
        }
    }

    function selectStartDate() {

      // var date_input=$('input[name="endDate"]'); //our date input has the name "date"
      // var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      // var options={
      //   format: 'yyyy-mm-dd',
      //   container: container,
      //   todayHighlight: true,

      //   autoclose: true,
      //   startDate: document.getElementById('startDate').value,
      // };
      // date_input.datepicker(options);


      var startDate = document.getElementById('startDate').value;
      
      var dd = startDate.substr(0, 2);
      var mm = startDate.substr(3, 2);
      var yy = startDate.substr(6, 4);
      var dateMDY = mm+'-'+dd+'-'+yy;
      
      var date = new Date(dateMDY);

      date.setDate(date.getDate() + 365);

      var month = (date.getMonth() + 1);
      if (parseInt(month) < 10){
          month = '0' + month;
      }

      var day = date.getDate();
      if (parseInt(day) < 10){
          day = '0' + day;
      }

      var expiryDate = day + '-' + month + '-' + date.getFullYear();
      document.getElementById('endDate').value = expiryDate;
    }

    $(function() {
    //ถ้าใช้งานจริง ส่วนนี้จะถูกเขียนขึ้น เป็นไฟล์ .js เมื่อมีการเพิ่ม/แก้ไข ข้อมูลสมาชิก
    var autoCompleteData = <?php echo $allEmp?>;
    //--

    if(!autoCompleteData) var autoCompleteData = new Array();
    $( "#Name" ).autocomplete({
      minLength: 0,
      source: autoCompleteData,
      focus: function( event, ui ) {
      $( "#Name" ).val( ui.item.label );
      return false;
      },
      select: function( event, ui ) {
      $( "#Name" ).val( ui.item.label );
      $( "#project-id" ).val( ui.item.value );
      return false;
      }
    })
    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li>" )
      .append( "<a>" + item.label + "</a>" )
      .appendTo( ul );
    };
    });

  function cancelOnclick() {
   window.location = 'actList.php';
  }

</script>

</body>
</html>