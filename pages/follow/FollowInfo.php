<?php

session_start();
require_once "../../ConnectDatabase/connectionDb.inc.php";
function convertDateThai2($strDate)
{
    $strYear = substr($strDate,0,4); //date("Y",strtotime($strDate1));
    $strMonth= substr($strDate,5,2); //date("n",strtotime($strDate));
    $strDay= substr($strDate,8,2); //date("j",strtotime($strDate));
//substr ตัดคำ พารามิเตอร์คือ อักษรที่ต้องการให้เริ่มแสดง,ต้องการให้แสดงกี่ตัวอักษร
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[intval($strMonth)];
    return "$strDay$strMonthThai$strYear";
}
$id = getIsset("id");
$cmd = getIsset("__cmd");
$action= getIsset("__action");

if (intval($id) > 0){
  //Update
  if ($cmd == 'save') {
    $value = array(
      // "status"=>getIsset('status')
      // ,"methodtype"=>getIsset('methodtype')
      "methodtype"=>'ติดตามแล้ว'
    );
        if ($conn->update("follow", $value, array("id" => $id))) {
          if ($action == 'act'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','act.php');
          }else if ($action == 'tax'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','tax.php');
          }else if ($action == 'insurance'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','insurance.php');
          }
        }else{
           alertMassage("ไม่สามารถบันทึกข้อมูลได้");
        }
  }else if ($cmd == 'SMS'){

    $tbl_Follow = $conn->select('follow', array('id' => getIsset('id')), true);
    $type = $tbl_Follow["type"];
    $end = convertDateThai2($tbl_Follow["endDate"]);

    $USERNAME = 'gritanon';
    $PASSWORD = '432a9d';
    $FROM = '0000';
    $TO = getIsset('Tel');
    $car = getIsset('license');
    $FName = getIsset('FName');
    
    $MESSAGE = 'สวัสดีค่ะคุณ'.$FName.'ขณะนี้บริการ'.$type.'ทะเบียนรถ'.$car.'จะครบกำหนดเมื่อวันที่'.$end.'ท่านสามารถนำรถเข้ามารับการ'.$type.'ได้ที่ตรอ.ช่างใหญ่เซอร์วิส105หมู่ที่2ต.เวียงยองอ.เมืองจ.ลำพูนติดต่อเราโทร053-512305';
    file_get_contents('http://www.thsms.com/api/rest?method=send&username='.$USERNAME.'&password='.$PASSWORD.'&from='.$FROM.'&to='.$TO.'&message='.$MESSAGE.'');

    $value = array(
      // "status"=>'ส่งข้อความ'
      // ,"methodtype"=>getIsset('methodtype')
      "methodtype"=>'ติดตามแล้ว'
    );
        if ($conn->update("follow", $value, array("id" => $id))) {
          if ($action == 'act'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','act.php');
          }else if ($action == 'tax'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','tax.php');
          }else if ($action == 'insurance'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','insurance.php');
          }

        }else{
           alertMassage("ไม่สามารถบันทึกข้อมูลได้");
        }
  }else if ($cmd == 'LETTER'){
    redirect('letter-print.php?id='.$id.'&actions='.$action.'&methodtype='.getIsset('methodtype'));

    $value = array(
      // "status"=>'ส่งจดหมาย'
      // ,"methodtype"=>getIsset('methodtype')
      "methodtype"=>'ติดตามแล้ว'
    );
        if ($conn->update("follow", $value, array("id" => $id))) {
          if ($action == 'act'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','act.php');
          }else if ($action == 'tax'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','tax.php');
          }else if ($action == 'insurance'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','insurance.php');
          }

        }else{
           alertMassage("ไม่สามารถบันทึกข้อมูลได้");
        }

  }else if ($cmd == 'TEL'){

    $value = array(
      "status"=>'โทร'
      // ,"methodtype"=>getIsset('methodtype')
      ,"methodtype"=>'ติดตามแล้ว'
    );
        if ($conn->update("follow", $value, array("id" => $id))) {
          if ($action == 'act'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','act.php');
          }else if ($action == 'tax'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','tax.php');
          }else if ($action == 'insurance'){
            alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','insurance.php');
          }

        }else{
           alertMassage("ไม่สามารถบันทึกข้อมูลได้");
        }
  }else {

    $tbl_Follow = $conn->select('follow', array('id' => getIsset('id')), true);

    if($tbl_Follow != null){
      $status = $tbl_Follow["status"];
      $methodtype = $tbl_Follow["methodtype"];

      if ($action == 'act'){
          $tbl_ = $conn->select('act', array('id' => $tbl_Follow["refID"]), true);
      }else if ($action == 'tax'){
        $tbl_ = $conn->select('tax', array('id' => $tbl_Follow["refID"]), true);
      }else if ($action == 'insurance'){
        $tbl_ = $conn->select('insurance', array('id' => $tbl_Follow["refID"]), true);
      }

      $cusID = $tbl_["cusID"];
      $carID = $tbl_["carID"];
      $Date = $tbl_["Date"];
      $type = $tbl_["type"];
      $Price = $tbl_["Price"];
      $startDate = convertDateThai($tbl_["startDate"]);
      $endDate = convertDateThai($tbl_["endDate"]);

      $tbl_car = $conn->select('car', array('id' => $carID), true);
      $license = $tbl_car["license"];
      $province_license = $tbl_car["province_license"];
      $registration = convertDateThai($tbl_car["registration"]);
      $typecar = $tbl_car["typecar"];
      $brand = $tbl_car["brand"];
      $generation = $tbl_car["generation"];
      $body_number = $tbl_car["body_number"];
      $serial_number = $tbl_car["serial_number"];
      $gas_number = $tbl_car["gas_number"];
      $fuel_type = $tbl_car["fuel_type"];
      $capacity = $tbl_car["capacity"];

      $tbl_cus = $conn->select('customer', array('id' => $cusID), true);
      $FName = $tbl_cus["FName"];
      $LName = $tbl_cus["LName"];
      $Address = $tbl_cus["Address"];
      $district = $tbl_cus["district"];
      $amphoe = $tbl_cus["amphoe"];
      $province = $tbl_cus["province"];
      $zipcode = $tbl_cus["zipcode"];
      $Tel = $tbl_cus["Tel"];
    }
  }
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ข้อมูลติดตามลูกค้า</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">ข้อมูลติดตามลูกค้า</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <form>
      <div class="container-fluid">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">ข้อมูลติดตามลูกค้า</h3>
          </div>
          <div class="col-12 table-responsive card-body">
            <b class="">รายละเอียดข้อมูลเจ้าของรถ</b>
            <hr>
            <div class="card-body">
              <input type="hidden" name="__action" value="<?php echo $action ?>">
              <input type="hidden" name="id" value="<?php echo $id ?>">
              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ชื่อ</label>
                <div class="col-10">
                  <input type="hidden" id="CusID"  placeholder="ชื่อ" class="form-control" name="cusID" value="<?php echo $cusID ?>" hidden>
                  <input type="hidden" id="CarID"  placeholder="รถ" class="form-control" name="carID" value="<?php echo $carID ?>" hidden>

                  <input type="text" id="CusFName" class="form-control" placeholder="ชื่อ" name="FName" value="<?php echo $FName; ?>" readonly readonly>
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">นามสกุล</label>
                <div class="col-10">
                  <input type="text" id="CusLName"  class="form-control" placeholder="นามสกุล" name="LName" value="<?php echo $LName; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">เบอร์โทร</label>
                <div class="col-10">
                  <input type="number" id="CusTel"  class="form-control" placeholder="เบอร์โทร" name="Tel" value="<?php echo $Tel; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ที่อยู่</label>
                <div class="col-10">
                <input type="text"  id="CusAddress" class="form-control" placeholder="ที่อยู่" name="Address" value="<?php echo $Address; ?> <?php echo $district; ?> <?php echo $amphoe; ?> <?php echo $province; ?> <?php echo $zipcode; ?>" readonly>
                </div>
              </div>

          </div>

          <hr>
            <b>รายละเอียดรถ</b>
          <hr>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ทะเบียนรถ</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ทะเบียนรถ" id="license" name="license" value="<?php echo $license; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">จังหวัด</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="จังหวัด" id="provincelicense" name="province_license" value="<?php echo $province_license; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">วันที่จดทะเบียน</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="วันที่จดทะเบียน" id="registration" name="registration" value="<?php echo $registration; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">รย.</label>
              <div class="col-8">
               <input type="text" class="form-control" placeholder="รย." id="typecar" name="typecar" value="<?php echo $typecar; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ยี่ห้อรถ</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ยี่ห้อรถ" id="brand" name="brand" value="<?php echo $brand; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">รุ่น</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="รุ่น" id="generation" name="generation" value="<?php echo $generation; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขตัวถัง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขตัวถัง" id="bodynumber" name="body_number" value="<?php echo $body_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขเครื่อง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขเครื่อง" id="serialnumber" name="serial_number" value="<?php echo $serial_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขถังแก๊ส</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขถังแก๊ส" id="gasnumber" name="gas_number" value="<?php echo $gas_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ชนิดเชื่อเพลิง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ชนิดเชื่อเพลิง" id="fueltype" name="fuel_type" value="<?php echo $fuel_type; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ขนาดเครื่องยนต์ (ซีซี)</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ขนาดเครื่องยนต์" id="capacity" name="capacity" value="<?php echo $capacity; ?>" readonly>
              </div>
            </div>

            <hr>
              <b>ข้อมูลติดตามลูกค้า</b>
            <hr>
            <!-- <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ประเภทการติดตาม</label>
              <div class="col-8">
                <select id="select" class="form-control" name="status" id="typeCompany" required>
                   <option value="">โปรดเลือกประเภทการติดตาม</option>
                   <option value="ส่งข้อความ" <?php echo ('ส่งข้อความ'==  $status) ? ' selected="selected"' : '';?>>ส่งข้อความ</option>
                   <option value="โทร" <?php echo ('โทร'==  $status) ? ' selected="selected"' : '';?>>โทร</option>
                    <option value="ส่งจดหมาย" <?php echo ('ส่งจดหมาย'==  $status) ? ' selected="selected"' : '';?>>ส่งจดหมาย</option>
                 </select>
              </div>
            </div> -->

              <!-- <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">สถานะการติดตาม</label>
                <div class="col-8">
                  <select id="select" class="form-control" name="methodtype" id="typeCompany" required>
                     <option value="">โปรดเลือกสถานะการติดตาม</option>
                     <option value="ติดตามแล้ว" <?php echo ('ติดตามแล้ว'==  $methodtype) ? ' selected="selected"' : '';?>>ติดตามแล้ว</option>
                     <option value="กำลังติดต่อ" <?php echo ('กำลังติดต่อ'==  $methodtype) ? ' selected="selected"' : '';?>>กำลังติดต่อ</option>
                 </select>
                </div>
              </div> -->

            <div class="form-group">
             <center>
                 <button type="submit" class="btn btn-primary" name="__cmd" value="SMS">ส่งข้อความ</button>
                 <button type="submit" class="btn btn-success" name="__cmd" value="TEL">บันทึกว่าโทร</button>
                 <button type="reset" class="btn btn-default" onclick="cancelOnclick('<?php echo $action; ?>')">ยกเลิก</button>
               </center>
           </div>

        <!-- /.card -->
      </div><!-- /.container-fluid -->
      </form>
    </section>
    <!-- /.content -->
  </div>

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datepicker/locales/bootstrap-datepicker.th.js"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker-thai.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
<script src="../../plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>

<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

<script>
  $(document).ready(function () {
    var date_input=$('input[name="startDate"]'); //our date input has the name "date"
         var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
         var options={
           format: 'yyyy-mm-dd',
           container: container,
           todayHighlight: true,
           autoclose: true,
         };
         date_input.datepicker(options);


      $('.datepicker').datepicker({
          format: 'dd/mm/yyyy',
          todayBtn: true,
          language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
          thaiyear: true              //Set เป็นปี พ.ศ.
      }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน

      $('#dataTables-example').DataTable({
          "oLanguage": {
              "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
              "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
              "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
              "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
              "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
              "sSearch": "ค้นหา :",
              "oPaginate": {
                  "sFirst": "เิริ่มต้น",
                  "sPrevious": "ก่อนหน้า",
                  "sNext": "ถัดไป",
                  "sLast": "สุดท้าย"
              }
          },
            responsive: true
        });
  });

  function addCustomer(id,FName,LName,Tel,Address,license,registration,
    typecar,brand,generation,serialnumber,gasnumber,fueltype,
    provincelicense,Bodynumber,cusID,capacity) {
        document.getElementById("CusID").value = cusID;
        document.getElementById("CusFName").value = FName;
        document.getElementById("CusLName").value = LName;
        document.getElementById("CusAddress").value = Address;
        document.getElementById("CusTel").value = Tel;
        document.getElementById("license").value = license;
        document.getElementById("registration").value = registration;
        document.getElementById("typecar").value = typecar;
        document.getElementById("brand").value = brand;
        document.getElementById("generation").value = generation;
        document.getElementById("serialnumber").value = serialnumber;
        document.getElementById("gasnumber").value = gasnumber;
        document.getElementById("fueltype").value = fueltype;
        document.getElementById("provincelicense").value = provincelicense;
        document.getElementById("bodynumber").value = Bodynumber;
        document.getElementById("CarID").value = id;
        document.getElementById("capacity").value = capacity;

        if (typecar == 'รย.12'){
            document.getElementById("price").value = "324";
        }else if (typecar == 'รย.1'){
              document.getElementById("price").value = "645";
        }else if (typecar == 'รย.2'){
            document.getElementById("price").value = "1282";
        }else if (typecar == 'รย.3'){
            document.getElementById("price").value = "967";
        }
    }

    function selectStartDate() {

      var date_input=$('input[name="endDate"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
        startDate: document.getElementById('startDate').value,
      };
      date_input.datepicker(options);

      var startDate = document.getElementById('startDate').value;
      var date = new Date(startDate);
      date.setDate(date.getDate() + 365) ;

      var month = (date.getMonth() + 1);
      if (parseInt(month) < 10){
          month = '0' + month;
      }
      var expiryDate = date.getFullYear() + '-' + month + '-' + date.getDate();
      document.getElementById('endDate').value = expiryDate;

    }

  function cancelOnclick(actionss) {

    if (actionss == 'act'){
      window.location = 'act.php';
    }else if (actionss == 'tax'){
      window.location = 'tax.php';
    }else if (actionss == 'insurance'){
     window.location = 'insurance.php';
    }
  }

  function reportPrint(id) {
    window.open("letter-print.php?id=" + id);
  }

</script>

</body>
</html>
