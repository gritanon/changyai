<?php
  session_start();
  require_once "../../ConnectDatabase/connectionDb.inc.php";

  $id = getIsset("id");

  $sql="SELECT f.endDate,c.license,c.province_license,cm.FName,cm.LName
        FROM follow f inner join car c on f.carID = c.id
        inner join customer cm on f.cusID = cm.id
        WHERE f.id = $id";
         $select_all = $conn->queryRaw($sql);
         $total = sizeof($select_all);
         foreach ($select_all as $row) ;

    $license = $row["license"];
    $province_license = $row["province_license"];
    $endDate = $row["endDate"];

    $FName = $row["FName"];
    $LName = $row["LName"];

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
      body{
        font-size:25px;
      }
  </style>
  <style>
    p.indent{ padding-left: 2em };
  </style>
  <style>
    #rcorners {
    border-radius: 25px;
    border: 2px solid black;
    padding: 20px;
    width: 100%;
    height: 100%;
  }
  </style>

</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
  <div class="container-fluid pl-1 pr-2">
    <!-- title row -->
    <div class="row">
      <div class="col-12">
        <h2 class="page-header">
          <img src="../../dist/img/changyai.png" height="230" width="300" alt="User Avatar" class="mr-2">
          สถานตรวจสภาพรถเอกชน ช่างใหญ่เซอร์วิส จ.ลำพูน
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <div class="row">
      <div class="col-7">
        <p class="pt-3">เรียนคุณ <u> <?php echo $FName ?>  <?php echo $LName ?> </u> ทะเบียนรถ <u> <?php echo $license ?>  
        <?php echo $province_license ?>  </u></p>
        <br>
        <p class="indent mb-0 pt-3">เนื่องจาก ตรอ. ช่างใหญ่เซอร์วิส ได้เปิดกิจการใหม่ขอ</p>
        <p>เชิญคุณลูกค้านำรถเข้ามาตรวจสภาพเพื่อนำไปต่อภาษีประจำปี และทำ พรบ. 3 2+ 3+ สอบถามฟรี
           กับ ตรอ. ช่างใหญ่เซอร์วิสก่อนวันที่<u> <?php echo convertDateThai($endDate) ?>  </u>
        </p>
        <p class="indent mb-0 pt-3">บัดนี้รถของท่านใกล้ถึงเวลาที่จะต้องนำรถมาตรวจเพื่อ</p>
        <p>ต่อภาษีของปีต่อไปโดยท่านสามารถนำรถเข้ามารับการตรวจเพื่อต่อภาษีก่อนกำหนดพร้อมทำ พรบ.
           โอนรถ อื่นๆ ได้ที่ร้านตามแผนที่และที่อยู่ ที่ระบุไว้ในจดหมาย
        </p>
        <p class="pt-3 mb-0 text-center">หากท่านได้ดำเนินการแล้วต้องขออภัยมา ณ ที่นี้</p>
        <p class="mb-0 text-center">ขอขอบพระคุณอย่างสูง</p>
        <p class="mb-0 text-center"> สถานตรวจสภาพรถ ช่างใหญ่เซอร์วิส โทร. 053-512305</p>
      </div>
      <!-- /.col -->
      <div class="col-5 m-auto pr-3 pl-3" id="rcorners">
         <h3 class="text-center">บริการพิเศษ</h3>
         <ul>
            <li>พรบ. รถเก๋ง , รถกระบะ</li>
            <li>จำหน่ายประกันประเภท 1  2  3   2+  3+  สอบถาม<b><i>ฟรี !</i></b></li>
            <li>ฝากต่อภาษี<b><i>ฟรี !</i></b></li>
            <li>ให้คำปรึกษาเคลมประกันภัย</li>
            <li>ให้คำปรึกษาเรื่อง ทะเบียนรถโอนรถ อื่นๆ</li>
            <li>ให้คำปรึกษาเคลมประกันภัย</li>
            <li>บริการเช็ครถ (น้ำมันเครื่อง, กรองอากาศ , น้ำกลั่น ,น้ำหม้อน้ำ , ลมยาง, เบรก <b><i>ฟรี !</i></b></li>
         </ul>
      </div>
      <!-- /.col -->
    </div>
    <img src="../../dist/img/cy-map.jpg" height="100%" width="100%" alt="User Avatar" class="pt-3">
    <div class="row">
      <!-- accepted payments column -->
      <div class="col-12 pt-5">
        <hr>
        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          ตรอ.ช่างใหญ่เซอร์วิส  ๑o๕ หมู่ที่ ๒ ต.เวียงยอง อ.เมือง จ.ลำพูน  ๕๑ooo
          โทรศัพท์ o๕๓-๕๑๒๓o๕, o๘๖-๘๘๕๑๗๔๗
        </p>
      </div>
    </div>
    <!-- /.row -->
  </div>
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
