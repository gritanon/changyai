
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Promotion Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php 
    session_start();
    include_once('../includes/check_sidebar.php') ?>
  <?php 

  require_once('../../php/connect.php');  
  $id=$_GET['id'];
  
  $sql = "SELECT name,detail,create_at,img
  FROM promotion WHERE id='".$id."'";
  $result = $conn->query($sql) or die($conn->error);
  $row = $result->fetch_assoc();


?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Promotion Management</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">Promotion Management</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title d-inline-block">แก้ไขโปรโมชั่น</h3>
        </div>
          
      <form action="update.php" method="post" enctype="multipart/form-data">  
        <input type="hidden" class="form-control" name="id" value="<?php echo $id ;?>">
        <!-- Modal for edite -->
        <div class="card-body table-responsive">
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                  <!-- general form elements -->

                      <div class="form-group">
                        <label>อัพโหลดรูปภาพบนสไลท์</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" id="fileName2" name="filUpload" onchange="readURL(this);validateFileType2();switchimg()">
                            <label class="custom-file-label" >Choose file</label>
                          </div>
                        </div>
                        <div id="myDIV">
                             <img class="img-responsive my-3 img-fluid d-block mx-auto" src="promotion/<?php echo $row['img'] ?>" alt=""  width="50%" height="50%">
                        </div>
                        <div id="myDIV2" style="display:none;">
                        <img class="img-responsive my-3 img-fluid d-block mx-auto" id="blah" src="#" alt="" width="50%" height="50%">
                        </div>

                        <label>ชื่อโปรโมชั่น</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="text" class="form-control" name="name" value="<?php echo $row['name']  ?>">
                          </div>

                        <!-- ckeditor -->
                        <div class="input-group mt-3">
                        <script src="ckeditor.js"></script>
                        <label>รายละเอียดโปรโมชั่น</label>
                        <textarea class="my-3" cols="80" id="detail2" name="detail" rows="10"><?php echo $row['detail']  ?></textarea>
                        </div>

                      </div>

                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary" >แก้ไข</button>
                <a href="index.php" class="btn btn-default">
                    กลับ
                </a> 
              </div>
            </div>
          </div>
        </div>
      </form>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>
  
</div>

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>

<script>
    $(function () {
      $('#dataTable').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });
    });


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
                $('#blah2').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    CKEDITOR.replace( 'detail2', {
      toolbar: [
      { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print' ] },
      { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
      { name: 'editing', items: [ 'Find', 'Replace' ] },
      '/',
      { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
      { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
      { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
      { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
      { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
      { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
      { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
      ]
    });


    function validateFileType2(){
      var fileName2 = document.getElementById("fileName2").value;
      var idxDot = fileName2.lastIndexOf(".") + 1;
      var extFile = fileName2.substr(idxDot, fileName2.length).toLowerCase();
      if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        return true;
      }else{
        alert("ไม่สามารถเลือกเอกสารอื่นได้ นอกจากไฟล์รูปภาพ jpg,jpeg,png");
        document.getElementById("fileName2").value = '';
        return false;
      }   
    }

    function switchimg() {
        document.getElementById("myDIV").style.display = "none";
        document.getElementById("myDIV2").style.display = "";
    }
</script>

</script>

</body>
</html>
