<?php

session_start();
require_once "../../ConnectDatabase/connectionDb.inc.php";

$id = getIsset("id");

if (intval($id) > 0){
  $tbl_insurance = $conn->select('insurance', array('id' => getIsset('id')), true);

  if($tbl_insurance != null){
    $cusID = $tbl_insurance["cusID"];
    $carID = $tbl_insurance["carID"];
    $Date = $tbl_insurance["Date"];
    $startDate = $tbl_insurance["startDate"];
    $endDate = $tbl_insurance["endDate"];
    $type = $tbl_insurance["type"];
    $protection = $tbl_insurance["protection"];
    $premium = $tbl_insurance["premium"];
    $insurance = $tbl_insurance["insurance"];
    $remark = $tbl_insurance["remark"];

    $tbl_car = $conn->select('car', array('id' => $carID), true);
    $license = $tbl_car["license"];
    $province_license = $tbl_car["province_license"];
    $registration = $tbl_car["registration"];
    $typecar = $tbl_car["typecar"];
    $brand = $tbl_car["brand"];
    $generation = $tbl_car["generation"];
    $body_number = $tbl_car["body_number"];
    $serial_number = $tbl_car["serial_number"];
    $gas_number = $tbl_car["gas_number"];
    $fuel_type = $tbl_car["fuel_type"];
    $capacity = $tbl_car["capacity"];

    $tbl_cus = $conn->select('customer', array('id' => $cusID), true);
    $FName = $tbl_cus["FName"];
    $LName = $tbl_cus["LName"];
    $Address = $tbl_cus["Address"];
    $Tel = $tbl_cus["Tel"];
  }
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
  <body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-6 m-auto">
        <h2 class="page-header align-bottom">
                ใบเสร็จรับเงิน
          </h2>
      </div>
      <div class="col-6">
        <t class=" float-right ml-5"><img src="../../dist/img/changyai.png" height="230" width="300" alt="User Avatar" class="mr-2">
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        ข้อมูลร้าน
        <address>
          <strong>ตรอ.ช่างใหญ่เซอร์วิส</strong><br>
          ที่อยู่ : 105 หมู่ 2 ถ.ลำพูน-ดอยติ ต.เวียงยอง อ.เมือง จ.ลำพูน (ก่อนถึงโรงเรียนใบบุญ) <br>
          เบอร์โทร : 053-512305<br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        ข้อมูลลูกค้า
        <address>
          <strong><?php echo $FName; ?>  </strong><br>
          ที่อยู่ : <?php echo $Address; ?> <br>
          เบอร์โทร : <?php echo $Tel; ?><br>
        </address>
      </div>

      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <b>วันที่ทำรายการ :  <?php echo $Date; ?></b><br>
        <br>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <hr>

    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
          <strong>ข้อมูลรถ</strong><br>
          ทะเบียนรถ : <?php echo $license; ?> <?php echo $province_license; ?> <br>
          วันที่จดทะเบียน : <?php echo convertDateThai($registration); ?><br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <address><br>
          รย. : <?php echo $typecar; ?> <br>
          ยี่ห้อรถ : <?php echo $brand; ?><br>
          รุ่น : <?php echo $generation; ?><br>
          ขนาดเครื่องยนต์ (ซีซี) : <?php echo $capacity; ?><br>
        </address>
      </div>

      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <address><br>
          ชนิดเชื่อเพลิง : <?php echo $fuel_type; ?> <br>
          หมายเลขตัวถัง : <?php echo $body_number; ?> <br>
          หมายเลขเครื่อง : <?php echo $serial_number; ?><br>
          หมายเลขถังแก๊ส : <?php echo $gas_number; ?><br>
        </address>
      </div>
      <!-- /.col -->
    </div>

    <hr>

    <strong>ข้อมูลการต่อประกัน</strong><br>
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
          วันเริ่มต้นการคุ้มครอง : <?php echo convertDateThai($startDate); ?> <br>
          วันสิ้นสุดการคุ้มครอง : <?php echo convertDateThai($endDate); ?><br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <address>
          บริษัทคุ้มครอง : <?php echo $type; ?> <br>
          ความคุ้มครอง : <?php echo $protection; ?><br>
        </address>
      </div>

      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <address>
          ทุนประกัน : <?php echo number_format($insurance,2); ?> <br>
          หมายเหตุ : <?php echo $remark; ?> <br>
        </address>
      </div>
      <!-- /.col -->
    </div>

      <strong>ยอดชำระค่าเบี้ยประกัน : </strong> <?php echo number_format($premium,2); ?> <strong>บาท</strong><br>
    <!-- /.row -->

    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->

<script src="../../plugins/jquery/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $("#gdRows tr td").each(function() {
            var cell = $.trim($(this).text());
        if (cell == 0) {
            $(this).parent().hide();
        }
    });
    });
</script>
</body>
</html>
