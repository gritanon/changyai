<?php

  session_start();
  require_once "../../ConnectDatabase/connectionDb.inc.php";


  $strExcelFileName="data.xlsx";

  header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
  header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
  header("Pragma:no-cache");

  $sql = "select * from customer c inner join car ca on c.id = ca.cusID";
  $select_all = $conn->queryRaw($sql);
  $total = sizeof($select_all);

?>

<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
<body>

<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
  <table x:str border=1 cellpadding=0 cellspacing=1 width=100% style="border-collapse:collapse">
    <tr>
      <td width="94" height="30" align="center" valign="middle" ><strong>ชื่อ</strong></td>
      <td width="200" align="center" valign="middle" ><strong>นามสกุล</strong></td>
      <td width="181" align="center" valign="middle" ><strong>เบอร์โทร</strong></td>
      <td width="181" align="center" valign="middle" ><strong>ที่อยู่</strong></td>
      <td width="181" align="center" valign="middle" ><strong>ตำบล</strong></td>
      <td width="181" align="center" valign="middle" ><strong>อำเภอ</strong></td>
      <td width="181" align="center" valign="middle" ><strong>จังหวัด</strong></td>
      <td width="181" align="center" valign="middle" ><strong>รหัส ปณ.</strong></td>
      <td width="181" align="center" valign="middle" ><strong>ทะเบียนรถ</strong></td>
      <td width="181" align="center" valign="middle" ><strong>จังหวัด</strong></td>
      <td width="181" align="center" valign="middle" ><strong>วันที่จดทะเบียน</strong></td>
      <td width="181" align="center" valign="middle" ><strong>รย.</strong></td>
      <td width="181" align="center" valign="middle" ><strong>ยี่ห้อรถ</strong></td>
      <td width="181" align="center" valign="middle" ><strong>รุ่น</strong></td>
      <td width="181" align="center" valign="middle" ><strong>หมายเลขตัวถัง</strong></td>
      <td width="181" align="center" valign="middle" ><strong>หมายเลขเครื่อง</strong></td>
      <td width="181" align="center" valign="middle" ><strong>หมายเลขถังแก๊ส</strong></td>
      <td width="181" align="center" valign="middle" ><strong>ชนิดเชื้อเพลิง</strong></td>
      <td width="181" align="center" valign="middle" ><strong>ขนาดเครื่องยนต์ (ซีซี)</strong></td>
    </tr>
  <?php
    $index = 0;
    foreach ($select_all as $result) {
        $index++;
        ?>
      <tr>
        <td align="center" valign="middle" ><?php echo $result['FName'];?></td>
        <td align="center" valign="middle" ><?php echo $result['LName'];?></td>
        <td align="center" valign="middle" ><?php echo $result['Tel'];?></td>
        <td align="center" valign="middle" ><?php echo $result['Address'];?></td>
        <td align="center" valign="middle" ><?php echo $result['district'];?></td>
        <td align="center" valign="middle" ><?php echo $result['amphoe'];?></td>
        <td align="center" valign="middle" ><?php echo $result['province'];?></td>
        <td align="center" valign="middle" ><?php echo $result['zipcode'];?></td>
        <td align="center" valign="middle" ><?php echo $result['license'];?></td>
        <td align="center" valign="middle" ><?php echo $result['province_license'];?></td>
        <td align="center" valign="middle" ><?php echo convertToDateThai($result['registration']);?></td>
        <td align="center" valign="middle" ><?php echo $result['typecar'];?></td>
        <td align="center" valign="middle" ><?php echo $result['brand'];?></td>
        <td align="center" valign="middle" ><?php echo $result['generation'];?></td>
        <td align="center" valign="middle" ><?php echo $result['body_number'];?></td>
        <td align="center" valign="middle" ><?php echo $result['serial_number'];?></td>
        <td align="center" valign="middle" ><?php echo $result['gas_number'];?></td>
        <td align="center" valign="middle" ><?php echo $result['fuel_type'];?></td>
        <td align="center" valign="middle" ><?php echo $result['capacity'];?></td>
      </tr>
      <?php
        }
      ?>
  }
}
?>
</table>

</div>

<script>
  window.onbeforeunload = function(){return false;};
  setTimeout(function(){window.close();}, 10000);
</script>

</body>
</html>
